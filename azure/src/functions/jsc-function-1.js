const { app } = require("@azure/functions");

app.http("jsc-function-1", {
  methods: ["GET", "POST"],
  authLevel: "anonymous",
  handler: async (request, context) => {
    context.log(`Http function processed request for url "${request.url}"`);

    const requestData = await request.json();

    const input = requestData.input;

    if (!input) {
      return { status: 400, body: "Please pass a string in the request body" };
    }
    const transformedString = input.toUpperCase();
    return { body: `Your string is, ${transformedString}!` };
  },
});
