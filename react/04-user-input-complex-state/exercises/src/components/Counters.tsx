interface CountersProps {
  counter: number;
  onCounterClick: () => void;
  onDeleteClick: () => void;
}

const Counters = ({
  counter,
  onCounterClick,
  onDeleteClick,
}: CountersProps) => {
  return (
    <div className="counter-container">
      <button onClick={onCounterClick}>{counter}</button>
      <button onClick={onDeleteClick}>Delete</button>
    </div>
  );
};

export default Counters;
