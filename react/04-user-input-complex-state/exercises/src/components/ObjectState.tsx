import { useState } from "react";

const ObjectState = () => {
  const [count, setCount] = useState({ input: "", count: 0 });

  const handleClick = () => {
    setCount({ ...count, count: count.count + 1, input: "" });
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCount({ ...count, input: e.target.value });
  };
  return (
    <div>
      <input value={count.input} onChange={handleInputChange} />
      <button onClick={handleClick}>{count.count}</button>
    </div>
  );
};

export default ObjectState;
