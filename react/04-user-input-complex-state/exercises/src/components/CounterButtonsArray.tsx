import { useState } from "react";

const CounterButtonsArray = () => {
  const [counterArray, setCounterArray] = useState([
    { id: 1, counter: 0 },
    { id: 2, counter: 0 },
    { id: 3, counter: 0 },
    { id: 4, counter: 0 },
  ]);

  const handleButtonClick = (id: number) => {
    setCounterArray((prevArray) =>
      prevArray.map((item) =>
        item.id === id ? { ...item, counter: item.counter + 1 } : item
      )
    );
  };

  return (
    <div>
      {counterArray.map((item) => (
        <button key={item.id} onClick={() => handleButtonClick(item.id)}>
          {item.counter}
        </button>
      ))}
    </div>
  );
};

export default CounterButtonsArray;
