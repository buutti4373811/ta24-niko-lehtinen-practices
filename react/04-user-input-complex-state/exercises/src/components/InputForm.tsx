import { useState } from "react";

const InputForm = () => {
  const [name, setName] = useState("Niko");
  return (
    <>
      <h3>{name}</h3>
      <input value={name} onChange={(e) => setName(e.target.value)}></input>
    </>
  );
};

export default InputForm;
