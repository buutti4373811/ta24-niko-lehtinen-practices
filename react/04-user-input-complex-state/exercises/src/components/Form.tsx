import { useState } from "react";

const Form = () => {
  const [inputText, setInputText] = useState("");
  const [displayText, setDisplayText] = useState("");

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setDisplayText(inputText);
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <h3>Your String is: {displayText}</h3>
        <input
          value={inputText}
          onChange={(e) => setInputText(e.target.value)}
        ></input>
        <button type="submit">Click me</button>
      </form>
    </div>
  );
};

export default Form;
