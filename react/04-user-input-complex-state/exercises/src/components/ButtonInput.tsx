import { useState } from "react";

const ButtonInput = () => {
  const [inputText, setInputText] = useState("");
  const [displayText, setDisplayText] = useState("");

  return (
    <>
      <h3>Your String is: {displayText}</h3>
      <input
        value={inputText}
        onChange={(e) => setInputText(e.target.value)}
      ></input>
      <button type="submit" onClick={() => setDisplayText(inputText)}>
        Click me
      </button>
    </>
  );
};

export default ButtonInput;
