import { useState } from "react";
import "./App.css";
import ButtonInput from "./components/ButtonInput";
import CounterButtonsArray from "./components/CounterButtonsArray";
import Counters from "./components/Counters";

import ExerciseDivider from "./components/ExerciseDivider";
import Form from "./components/Form";
import Input from "./components/Input";
import ObjectState from "./components/ObjectState";
import { v4 as uuidv4 } from "uuid";

function App() {
  const [counters, setCounters] = useState([
    { id: "1", counter: 0 },
    { id: "2", counter: 0 },
    { id: "3", counter: 0 },
    { id: "4", counter: 0 },
  ]);

  const [minimumCounter, setMinimumCounter] = useState(0);

  const [toggleFiltering, setToggleFiltering] = useState(false);

  const fileteredCounters = counters.filter(
    (counter) => counter.counter >= minimumCounter
  );

  const incrementCounter = (id: string) => {
    setCounters((prevCounters) =>
      prevCounters.map((counter) =>
        counter.id === id
          ? { ...counter, counter: counter.counter + 1 }
          : counter
      )
    );
  };

  const addNewCounter = () => {
    setCounters((prevCounters) => [
      ...prevCounters,
      { id: uuidv4(), counter: 0 },
    ]);
  };

  const deleteCounter = (id: string) => {
    setCounters((prevCounters) =>
      prevCounters.filter((counter) => counter.id !== id)
    );
  };
  console.log(toggleFiltering);
  return (
    <div className="app-container">
      <ExerciseDivider exercise="Exercise 1 User Input" />
      <Input />
      <ExerciseDivider exercise="Exercise 2 User Input with submit" />
      <ButtonInput />
      <ExerciseDivider exercise="Exercise 3 Form Input" />
      <Form />
      <ExerciseDivider exercise="Exercise 4 Object state" />
      <ObjectState />
      <ExerciseDivider exercise="Exercise 5-6-7 Array of counters & Mapped aray of counters" />
      <CounterButtonsArray />
      <ExerciseDivider exercise="Exercise 8 Counters as components" />
      <div>
        <input
          type="checkbox"
          checked={toggleFiltering}
          onChange={() => setToggleFiltering(!toggleFiltering)}
        />
        <label>Toggle filtering</label>
        {fileteredCounters.map((counter) => (
          <Counters
            key={counter.id}
            counter={counter.counter}
            onCounterClick={() => incrementCounter(counter.id)}
            onDeleteClick={() => deleteCounter(counter.id)}
          />
        ))}
      </div>
      <button onClick={addNewCounter}>Add new counter</button>
      {toggleFiltering && (
        <input
          type="text"
          value={minimumCounter}
          onChange={(e) => setMinimumCounter(Number(e.target.value))}
        />
      )}
    </div>
  );
}

export default App;
