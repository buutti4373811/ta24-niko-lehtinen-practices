import { useEffect, useState } from "react";
import "./App.css";
import ExerciseDivider from "./components/ExerciseDivider";
import TodoNote from "./components/TodoNote";
import InputForm from "./components/InputForm";
import todoService from "./todoService";
interface Todo {
  id: string;
  text: string;
  complete: boolean;
}

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [searchQuery, setSearchQuery] = useState<string>("");

  const [fetchData, setFetchData] = useState(true);

  useEffect(() => {
    if (fetchData) {
      todoService.getAllTodos().then((initialTodos) => {
        setTodos(initialTodos);
        setFetchData(false);
      });
    }
  }, [fetchData]);

  const addTodo = (text: string) => {
    todoService.createTodo(text).then((newTodo) => {
      setTodos([...todos, newTodo]);
      setFetchData(true);
    });
  };

  const toggleCompletion = async (id: string) => {
    try {
      const updatedTodo = await todoService.toggleTodoCompletion(id);

      setTodos((prevTodos) =>
        prevTodos.map((todo) =>
          todo.id === updatedTodo.id
            ? { ...todo, complete: updatedTodo.complete }
            : todo
        )
      );

      setFetchData(true);
    } catch (error) {
      console.error("Error toggling todo completion:", error);
    }
  };

  const removeTodo = async (id: string) => {
    const deleteTodo = async (id: string) => {
      try {
        await todoService.deleteTodo(id);
        setFetchData(true);
      } catch (error) {
        console.error("Error deleting todo:", error);
      }
    };
    setTodos(todos.filter((todo) => todo.id !== id));
    await deleteTodo(id);
  };

  const editTodo = async (id: string, newText: string) => {
    const modifyTodo = await todoService.modifyTodo(id, newText);
    setTodos(
      todos.map((todo) =>
        modifyTodo.id === id ? { ...todo, text: newText } : todo
      )
    );
    setFetchData(true);
  };

  const filteredTodos = searchQuery
    ? todos.filter((todo) =>
        todo.text.toLowerCase().includes(searchQuery.toLowerCase())
      )
    : todos;

  return (
    <div className="app-container">
      <ExerciseDivider exercise="Assignment 1 Displaying the notes" />
      <input
        type="text"
        placeholder="Search notes..."
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
      />

      <div className="todo-container">
        {filteredTodos.map((todo) => (
          <TodoNote
            todo={todo}
            toggleCompletion={() => toggleCompletion(todo.id)}
            editTodo={editTodo}
            removeTodo={() => removeTodo(todo.id)}
          />
        ))}
      </div>

      <InputForm addTodo={addTodo} />
    </div>
  );
}

export default App;
