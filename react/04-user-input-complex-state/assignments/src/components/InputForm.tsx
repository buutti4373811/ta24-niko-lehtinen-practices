import { useState } from "react";

interface InputFormProps {
  addTodo: (text: string) => void;
}

const InputForm = ({ addTodo }: InputFormProps) => {
  const [newTodo, setNewTodo] = useState("");

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    addTodo(newTodo);
    setNewTodo("");
  };
  return (
    <form onSubmit={onSubmit}>
      <input
        type="text"
        placeholder="Add a new note"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
      />
      <button type="submit">Submit</button>
    </form>
  );
};

export default InputForm;
