import { useState } from "react";

interface TodoNoteProps {
  todo: {
    id: string;
    text: string;
    complete: boolean;
  };
  toggleCompletion: () => void;
  editTodo: (id: string, newText: string) => void;
  removeTodo: () => void;
}

const TodoNote = ({
  todo,
  toggleCompletion,
  editTodo,
  removeTodo,
}: TodoNoteProps) => {
  const [editMode, setEditMode] = useState(false);
  const [editedText, setEditedText] = useState(todo.text);

  const handleSave = () => {
    editTodo(todo.id, editedText);
    setEditMode(false);
  };
  return (
    <div className={`todoNote ${todo.complete ? "todo-completed" : ""}`}>
      <input
        type="checkbox"
        checked={todo.complete}
        onChange={toggleCompletion}
      />
      {!editMode ? (
        <p className="todo-text">{todo.text}</p>
      ) : (
        <input
          type="text"
          value={editedText}
          onChange={(e) => setEditedText(e.target.value)}
        />
      )}
      {!editMode ? (
        <button onClick={() => setEditMode(!editMode)}>Edit Note</button>
      ) : (
        <button onClick={handleSave}>Save</button>
      )}
      <button onClick={removeTodo}>X</button>
    </div>
  );
};

export default TodoNote;
