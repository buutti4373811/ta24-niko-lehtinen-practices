interface ExerciseDividerProps {
  exercise: string;
}

const ExerciseDivider = ({ exercise }: ExerciseDividerProps) => {
  return <h1 className="exercise-divider">{exercise}</h1>;
};

export default ExerciseDivider;
