import axios from "axios";

const baseUrl = "http://localhost:5000/todos";
const getAllTodos = async () => {
  try {
    const request = axios.get(baseUrl);
    const response = await request;
    return response.data;
  } catch (error) {
    console.error("Error fetching todos:", error);
    throw error;
  }
};

const createTodo = async (text: string) => {
  try {
    const response = await axios.post(baseUrl, { text });
    return response.data;
  } catch (error) {
    console.error("Error creating todo:", error);
    throw error;
  }
};
const toggleTodoCompletion = async (id: string) => {
  try {
    const response = await axios.put(`${baseUrl}/${id}`);

    return response.data;
  } catch (error) {
    console.error("Error toggling todo completion:", error);
    throw error;
  }
};

const modifyTodo = async (id: string, text: string) => {
  try {
    const response = await axios.put(`${baseUrl}/${id}`, { text });
    return response.data;
  } catch (error) {
    console.error("Error modifying todo:", error);
    throw error;
  }
};

const deleteTodo = async (id: string) => {
  try {
    const response = await axios.delete(`${baseUrl}/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error deleting todo:", error);
    throw error;
  }
};

export default {
  getAllTodos,
  createTodo,
  modifyTodo,
  toggleTodoCompletion,
  deleteTodo,
};
