import express from "express";
import fs from "fs";
import cors from "cors";
import { v4 as uuidv4 } from "uuid";

const app = express();

const notes = JSON.parse(
  fs.readFileSync("server/notes-db.json", "utf-8").toString()
);

app.use(express.json());
app.use(cors());

app.get("/todos", (_req: express.Request, res: express.Response) => {
  res.json(notes);
});

app.post("/todos", (req: express.Request, res: express.Response) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).json({ error: "Note text is required" });
  }
  const newTodo = {
    id: uuidv4(),
    text,
    complete: false,
  };

  notes.push(newTodo);

  fs.writeFileSync("server/notes-db.json", JSON.stringify(notes));

  res.json(notes);
});

app.put("/todos/:id", (req: express.Request, res: express.Response) => {
  const id = req.params.id;
  const note = notes.find((note: { id: string }) => note.id === id);

  const { text } = req.body;

  if (!note) {
    return res.status(404).json({ error: "Note not found" });
  }

  if (text) {
    note.text = text;
  }

  note.complete = !note.complete;

  fs.writeFileSync("server/notes-db.json", JSON.stringify(notes));

  res.json(notes);
});

app.delete("/todos/:id", (req: express.Request, res: express.Response) => {
  const id = req.params.id;
  const noteIndex = notes.findIndex((note: { id: string }) => note.id === id);

  if (noteIndex === -1) {
    return res.status(404).json({ error: "Note not found" });
  }

  notes.splice(noteIndex, 1);

  fs.writeFileSync("server/notes-db.json", JSON.stringify(notes));

  res.json(notes);
});

app.listen(5000, () => {
  console.log("Listening to port 5000");
});
