interface TypeComponentProps {
  numberOne: number;
  numberTwo: number;
}

const TypeComponent = ({ numberOne, numberTwo }: TypeComponentProps) => {
  const sum = (num1: number, num2: number) => num1 + num2;

  const result = sum(numberOne, numberTwo);
  return (
    <>
      <h1>here is result from sum of two numbers {result}</h1>;
      {result > 10 ? (
        <h3>Result is greater than 10</h3>
      ) : (
        <h3>Result is less than 10</h3>
      )}
    </>
  );
};

export default TypeComponent;
