import TypeComponent from "./TypeComponent";

function App() {
  return (
    <>
      <h1>Hello World</h1>

      <TypeComponent numberOne={1} numberTwo={2} />
      <TypeComponent numberOne={12} numberTwo={22} />
      <TypeComponent numberOne={1} numberTwo={7} />
      <TypeComponent numberOne={4} numberTwo={5} />
    </>
  );
}

export default App;
