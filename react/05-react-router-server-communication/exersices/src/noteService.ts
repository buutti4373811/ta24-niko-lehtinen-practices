import axios from "axios";
import type { Note } from "./components/Notes";

const baseUrl = "http://localhost:3000/notes";

const getAll = async () => {
  const request = axios.get(baseUrl);
  const response = await request;
  return response.data;
};

const create = async (newObject: Note) => {
  const request = axios.post(baseUrl, newObject);
  const response = await request;
  return response.data;
};

const update = async (id: number | string | undefined, newObject: Note) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  const response = await request;
  return response.data;
};

export default { getAll, create, update };
