import "./App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Links from "./components/Links";
import Notes from "./components/Notes";

function App() {
  return (
    <div className="app-container">
      <Router>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/links">Links</Link>
            </li>
          </ul>
        </nav>
        <Notes />
        <Routes>
          <Route path="/links" element={<Links />} />
          <Route path="/about" element={<About />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
