interface NoteProps {
  noteObj: {
    id?: number | undefined | string;
    content: string;
    date: string;
    important: boolean;
  };
}

const Note = ({ noteObj }: NoteProps) => {
  const { content, important } = noteObj;
  return (
    <div className={`note ${important ? "important-note" : ""}`}>{content}</div>
  );
};

export default Note;
