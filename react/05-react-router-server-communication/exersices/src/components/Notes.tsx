import { useEffect, useState } from "react";
import Note from "./Note";
import noteService from "../noteService";

export interface Note {
  id?: number | undefined | string;
  content: string;
  date: string;
  important: boolean;
}

const Notes = () => {
  const [notes, setNotes] = useState<Note[]>([]);

  // GET all notes from the server
  useEffect(() => {
    noteService.getAll().then((initialNotes) => {
      setNotes(initialNotes);
    });
  }, []);

  // POST a new note to the server
  const postToServer = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const newNote = {
      content: "note text",
      date: new Date().toISOString(),
      important: Math.random() > 0.5,
    };

    noteService.create(newNote).then((createdNote) => {
      setNotes([...notes, createdNote]);
    });
  };

  // PUT request to update the importance of a note
  const toggleImportance = (id: number | string | undefined) => {
    const note = notes.find((note) => note.id === id);
    const changedNote: Note = {
      ...note!,
      important: !note?.important,
      content: note!.content!,
      date: note!.date!,
    };

    noteService.update(id, changedNote).then((returnedNote) => {
      setNotes(notes.map((note) => (note.id !== id ? note : returnedNote)));
    });
  };

  return (
    <div>
      <h1>Notes</h1>
      <button onClick={postToServer}>Add Note</button>
      <ul className="notes-container">
        {notes.map((note: Note) => (
          <li key={note.id}>
            <button onClick={() => toggleImportance(note.id)}>Toggle</button>
            <Note noteObj={note} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Notes;
