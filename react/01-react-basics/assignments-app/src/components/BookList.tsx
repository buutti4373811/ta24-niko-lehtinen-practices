import { useState } from "react";

const BookList = () => {
  const [title, setTitle] = useState("");
  const [pages, setPages] = useState("");
  const [books, setBooks] = useState([
    {
      title: "Dune",
      pages: 412,
    },
    {
      title: "The Eye of the World",
      pages: 782,
    },
  ]);

  const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };

  const handlePagesChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPages(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setBooks([...books, { title, pages: parseInt(pages) }]);
    setTitle("");
    setPages("");
  };

  return (
    <div className="book-list-container">
      <h1 className="book-list-title">Book List Service</h1>
      <p>Books:</p>
      <ul className="book-list-books">
        {books.map((book) => (
          <li key={book.title} className="book-list-book">
            <p>
              {book.title} ({book.pages} pages)
            </p>
          </li>
        ))}
      </ul>
      <p>Add new Book</p>
      <form onSubmit={handleSubmit} className="book-list-add-book-form">
        <label htmlFor="title">Title:</label>
        <input
          type="text"
          name="title"
          value={title}
          onChange={handleTitleChange}
        />
        <label htmlFor="pages">Pages:</label>
        <input
          type="number"
          name="pages"
          value={pages}
          onChange={handlePagesChange}
        />
        <button type="submit">Add Book</button>
      </form>
    </div>
  );
};

export default BookList;
