interface greetingsProps {
  name: string;
  age: number;
}

const Greetings = ({ name, age }: greetingsProps) => {
  return (
    <h1 className="first">
      Hello my name is {name} and I am {age} years old!
    </h1>
  );
};

export default Greetings;
