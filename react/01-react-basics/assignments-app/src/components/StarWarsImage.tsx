const StarWarsImage = () => {
  return (
    <div className="image-container">
      <img src="/r2d2.jpg" alt="" />
      <div>
        <h2>Hello, I am R2-D2</h2>
        <p>BeeYoop BeeDeepBoom Weeop DEEpaEEya</p>
      </div>
    </div>
  );
};

export default StarWarsImage;
