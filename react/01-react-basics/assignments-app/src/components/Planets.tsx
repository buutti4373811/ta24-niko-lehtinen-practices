interface PlanetsProps {
  planets: { name: string; climate: string }[];
}

const Planets = (planets: PlanetsProps) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Planets</th>
        </tr>
      </thead>
      <tbody>
        {planets.planets.map((planet, index) => (
          <tr key={index}>
            <td>{planet.name}</td>
            <td>{planet.climate}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Planets;
