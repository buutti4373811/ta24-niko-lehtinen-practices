import "./App.css";
import BookList from "./components/BookList";
import Greetings from "./components/Greetings";
import Planets from "./components/Planets";
import StarWarsImage from "./components/StarWarsImage";

function App() {
  const fullName = "Niko Lehtinen";
  const age = 36;

  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic" },
  ];

  return (
    <>
      <h1 className="assignment">Assignment 1</h1>
      <Greetings name={fullName} age={age} />
      <h1 className="assignment">Assignment 2</h1>
      <StarWarsImage />
      <h1 className="assignment">Assignment 3</h1>
      <Planets planets={planetList} />
      <h1 className="assignment">Assignment 4 (wihtout bootstrap)</h1>
      <BookList />
    </>
  );
}

export default App;
