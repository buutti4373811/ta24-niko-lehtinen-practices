import Hello from "./components/Hello.tsx";
import List from "./components/List.tsx";

function App() {
  const currentYear = new Date().getFullYear();
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];
  return (
    <>
      <Hello name="Niko" year={currentYear} />
      <Hello name="Joonas" year={currentYear} />
      <Hello name="Kalle" year={currentYear} />
      <List list={namelist} />
    </>
  );
}

export default App;
