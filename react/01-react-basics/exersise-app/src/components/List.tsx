interface ListProps {
  list: string[];
}

const List = ({ list }: ListProps) => {
  return list.map((name, index) => (
    <p
      key={name}
      style={{
        fontWeight: index % 2 === 0 ? "bold" : "normal",
        fontStyle: index % 2 !== 0 ? "italic" : "normal",
      }}
    >
      {name}
    </p>
  ));
};

export default List;
