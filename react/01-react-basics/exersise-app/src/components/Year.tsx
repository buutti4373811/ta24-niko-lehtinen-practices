interface YearProps {
  year: number;
}
const Year = ({ year }: YearProps) => {
  return <h2>It is year {year}</h2>;
};

export default Year;
