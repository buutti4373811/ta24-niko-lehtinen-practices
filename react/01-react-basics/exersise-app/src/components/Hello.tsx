import Year from "./Year";

interface HelloProps {
  name: string;
  year: number;
}

const Hello: React.FC<HelloProps> = ({ name, year }) => {
  return (
    <>
      <h1>{name}</h1>
      <Year year={year} />
    </>
  );
};

export default Hello;
