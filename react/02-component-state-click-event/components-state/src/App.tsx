import "./App.css";
import CounterButtons from "./components/CounterButtons";
import ExerciseDivider from "./components/ExerciseDivider";
import Family from "./components/Family";

export interface PersonType {
  name: string;
  age: number;
}

function App() {
  const people: PersonType[] = [
    { name: "Alice", age: 30 },
    { name: "Bob", age: 35 },
    { name: "Charlie", age: 40 },
    { name: "Donna", age: 45 },
  ];

  return (
    <div className="app-container">
      <ExerciseDivider exercise="Exercise 1 & 2: Arrays & props" />
      <Family people={people} />
      <ExerciseDivider exercise="Exercise 3 & 4 & 5: Counters" />
      <CounterButtons />
    </div>
  );
}

export default App;
