interface CounterButtonProps {
  value: number;
  onChange: (value: number) => void;
}

const CounterButton = ({ value, onChange }: CounterButtonProps) => {
  const handleClick = () => {
    onChange(value + 1);
  };

  return <button onClick={handleClick}>{value}</button>;
};

export default CounterButton;
