import "./person.css";
interface PersonProps {
  name: string;
  age: number;
}

const Person = ({ name, age }: PersonProps) => {
  return (
    <p className="persons">
      Persons name is : <span>{name}</span> and he/she is <span>{age}</span>{" "}
      year old
    </p>
  );
};

export default Person;
