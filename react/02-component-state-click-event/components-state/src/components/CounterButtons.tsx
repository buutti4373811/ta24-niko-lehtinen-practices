import CounterButton from "../CounterButton";
import "./counterButton.css";
import { useState } from "react";

const CounterButtons = () => {
  const [counter1, setCounter1] = useState(0);
  const [counter2, setCounter2] = useState(0);
  const [counter3, setCounter3] = useState(0);
  const [showCounterButtons, setShowCounterButtons] = useState(true);

  const sum = counter1 + counter2 + counter3;

  return (
    <>
      <div>
        {showCounterButtons && (
          <>
            <CounterButton value={counter1} onChange={setCounter1} />
            <CounterButton value={counter2} onChange={setCounter2} />
            <CounterButton value={counter3} onChange={setCounter3} />
          </>
        )}
      </div>
      <button
        className="show-hide-button"
        onClick={() =>
          setShowCounterButtons((showCounterButtons) => !showCounterButtons)
        }
      >
        {showCounterButtons ? "Hide" : "Show"}
      </button>
      <h3 className="peoples">Total: {sum}</h3>
    </>
  );
};

export default CounterButtons;
