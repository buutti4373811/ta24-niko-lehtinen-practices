import { PersonType } from "../App";
import Person from "./Person";
import "./family.css/";

interface peopleProps {
  people: PersonType[];
}

const Family = ({ people }: peopleProps) => {
  return (
    <>
      <h3 className="peoples">Peoples</h3>
      {people.map((person) => (
        <Person key={person.name} name={person.name} age={person.age} />
      ))}
    </>
  );
};

export default Family;
