import { useState } from "react";

const ToggleButton = () => {
  const [isTextVisible, setIsTextVisible] = useState(true);

  return (
    <div className="text-container">
      <button
        onClick={() => setIsTextVisible((isTextVisible) => !isTextVisible)}
      >
        Toggle text
      </button>
      {isTextVisible && <p>Fear is the path to the darkside</p>}
    </div>
  );
};

export default ToggleButton;
