import { useState } from "react";

interface DissapearingButtonProps {
  number: number;
}

const DissapearingButton = ({ number }: DissapearingButtonProps) => {
  const [hideButton, setHideButton] = useState(false);

  return (
    <div>
      {!hideButton && (
        <button onClick={() => setHideButton((hideButton) => !hideButton)}>
          button : {number}
        </button>
      )}
    </div>
  );
};

export default DissapearingButton;
