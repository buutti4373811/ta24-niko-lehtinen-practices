import DissapearingButton from "./DissapearingButton";

const Buttons = () => {
  return (
    <div className=" buttons-container">
      <DissapearingButton number={1} />
      <DissapearingButton number={2} />
      <DissapearingButton number={3} />
      <DissapearingButton number={4} />
      <DissapearingButton number={5} />
    </div>
  );
};

export default Buttons;
