import { useEffect, useState } from "react";

interface BingoItemProps {
  name: string;
  i: number;
}
const clickArray: number[] = [];
const BingoItem = ({ name, i }: BingoItemProps) => {
  const [isClicked, setIsClicked] = useState(false);
  const [bingo, setBingo] = useState(false);

  useEffect(() => {
    const bingoConditions: number[][] = [
      [0, 1, 2, 3, 4], // Horizontal line 1
      [5, 6, 7, 8, 9], // Horizontal line 2
      [10, 11, 12, 13, 14], // Horizontal line 3
      [15, 16, 17, 18, 19], // Horizontal line 4
      [20, 21, 22, 23, 24], // Horizontal line 5
      [0, 5, 10, 15, 20], // Vertical line 1
      [1, 6, 11, 16, 21], // Vertical line 2
      [2, 7, 12, 17, 22], // Vertical line 3
      [3, 8, 13, 18, 23], // Vertical line 4
      [4, 9, 14, 19, 24], // Vertical line 5
      [0, 6, 12, 18, 24], // Diagonal from top-left
      [4, 8, 12, 16, 20], // Diagonal from top-right
    ];

    // Check if any of the bingo conditions are met
    //some method returns true if at least one element in the array satisfies the condition
    const isBingo = bingoConditions.some(
      (condition) => condition.every((index) => clickArray.includes(index)) // condition.every method returns true if all elements in the array satisfy the condition are clicked by the user
    );

    if (isBingo) {
      console.log("Bingo!");
      setBingo(true);
    }
  }, [isClicked, bingo]);

  const handleClick = () => {
    setIsClicked(true);
    clickArray.push(i);
  };

  return (
    <>
      <div
        onClick={handleClick}
        className={"bingo-item-container" + (isClicked ? " active" : "")}
      >
        <p>{name}</p>
      </div>
      {bingo && <span>Bingo!</span>}
    </>
  );
};

export default BingoItem;
