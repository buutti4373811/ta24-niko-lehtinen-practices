import "./App.css";
import Bingo from "./components/Bingo";
import Buttons from "./components/Buttons";
import ExerciseDivider from "./components/ExerciseDivider";
import ToggleButton from "./components/ToggleButton";

function App() {
  return (
    <div className="app-container">
      <ExerciseDivider exercise="Assignment 1" />
      <ToggleButton />
      <ExerciseDivider exercise="Assignment 2" />
      <Buttons />
      <ExerciseDivider exercise="Assignment 3" />
      <Bingo />
    </div>
  );
}

export default App;
