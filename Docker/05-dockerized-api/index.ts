// index.js
import express from "express";

const server = express();

const version = process.env.VERSION;

server.get("/", (request: express.Request, response: express.Response) => {
  response.send("'Hello from Docker!");
});

server.get(
  "/version",
  (request: express.Request, response: express.Response) => {
    response.send("API version: " + version);
  }
);
server.get(
  "/secret",
  (request: express.Request, response: express.Response) => {
    const secret = process.env.SECRET;
    if (!secret) {
      return response.status(500).send("Secret not provided.");
    }
    response.send(`Secret message: ${secret}`);
  }
);

server.listen(3000, () => {
  console.log("API running @ 3000");
});
