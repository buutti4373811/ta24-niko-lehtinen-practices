//prettier-ignore
function fibonacci(n) {
    if (n < 0) {
        return "Only positive numbers allowed";
    } else if (n === 0) {
        return [];
    }

    const fibonacciSequence = [0, 1];
    let n1 = 0;
    let n2 = 1;

    for (let i = 2; i < n; i++) {
        const sum = n1 + n2;
        fibonacciSequence.push(sum);
        n1 = n2;
        n2 = sum;
    }

    return fibonacciSequence;
}

// Example usage:
console.log(fibonacci(10)); // Output the first 10 Fibonacci numbers
