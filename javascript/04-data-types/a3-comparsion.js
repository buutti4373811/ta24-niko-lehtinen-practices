const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);
// a) ansver false
// b) ansver boolean
//c)

const avgGradesClass1 = (9 + 6 + 9) / 3;
const avgGradesClass2 = (7 + 10 + 5) / 3;

console.log(avgGradesClass1);
console.log(avgGradesClass2);
// prints whether the first class got a higher average score than the second class
// i did not figure what was intended way to archive this result as if/else or ternary operator hasnt been taught yet

console.log(
  avgGradesClass1 > avgGradesClass2
    ? "Class 1 has higher average grades"
    : "Class 2 has higher average grades"
);
