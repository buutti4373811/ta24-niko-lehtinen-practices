const book1 = {
  name: "dune",
  pageCount: 412,
  hasReaded: false,
};
const book2 = {
  name: "The eye of the world",
  pageCount: 782,
  hasReaded: true,
};
book1.hasReaded = true;
book2.hasReaded = false;
console.log(book1, book2);

const books = [book1, book2];
console.log(books);
