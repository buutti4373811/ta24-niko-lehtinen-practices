const myData = {
  name: "Niko",
  age: 36,
  likePineAppleOnPizza: false,
};

myData.favouriteMovie = "The Matrix";
myData.age = 37;
console.log(myData); // has {} curly braces

console.log(
  typeof myData.age,
  typeof myData.likePineAppleOnPizza,
  typeof myData.favouriteMovie,
  typeof myData.name
);

console.log(typeof myData);
