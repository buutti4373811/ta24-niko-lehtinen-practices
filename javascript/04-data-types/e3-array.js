// I Skipped few exercises, because i already have good understanding of them.

const fruits = ["apple", "banana", "orange", "strawberry", "kiwi"];

console.log(fruits.length);
fruits[0] = "Pear";
console.log(fruits);

fruits.push("Pineapple"); // Add Pineapple to the end of the array

console.log(fruits.length);

fruits.pop(); // Remove the last element from the array
console.log(fruits.length);

console.log(fruits); // [] inside is the values

console.log(typeof fruits); // object
