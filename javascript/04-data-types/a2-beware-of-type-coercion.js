let number;
const result1 = 10 + number;
console.log(typeof number);
console.log(result1);

number = null;
const result2 = 10 + number;

console.log(result2);
console.log(typeof null);
// a) so the type of number is undefined before its assigned a value of 0 aka null in this case therefore the result is NaN before the assignment

// b) What are the values of c, d, and e in the program below? Why are d and e different from each other?

const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;
console.log(c, d, e);

// ansver when boolean (a,b) is converted to number and it value is true it becomes 1 and when it is false it becomes 0 that explains the difference between d and e the + operator is used in this case to convert the boolean to number
