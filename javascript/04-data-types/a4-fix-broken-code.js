const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y: 11, hitpoints: 90 };
const damage = 15;

{
  let treeHitpointsLeft;
  let rockHitpointsLeft;

  const hitpointsRock = rock.hitpoints;
  const hitpointsTree = tree.hitpoints;
  rockHitpointsLeft = hitpointsRock - damage;
  console.log("Rock hitpoints left: " + rockHitpointsLeft);

  treeHitpointsLeft = hitpointsTree - damage;

  console.log("Tree hitpoints left: " + treeHitpointsLeft);
}
// fixed issue was hitpoint being assigned for both three and rock at the same time. so i gave them theyr own variables
