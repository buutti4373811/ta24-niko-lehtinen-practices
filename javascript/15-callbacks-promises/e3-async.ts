export {};

interface ValueObject {
  value: number;
}

const getValue = async function (): Promise<ValueObject> {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res({ value: Math.random() });
    }, Math.random() * 1500);
  });
};
// A) with async/await
const values = async () => {
  const value1: ValueObject = await getValue();
  const value2: ValueObject = await getValue();

  console.log(`A) Value 1 is ${value1.value} and value 2 is ${value2.value}`);
};

values();

// B) with promise.then
getValue().then((value1) => {
  getValue().then((value2) => {
    console.log(`B) Value 1 is ${value1.value} and value 2 is ${value2.value}`);
  });
});
