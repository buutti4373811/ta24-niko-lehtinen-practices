// A )
const waitFor = async (milliseconds: number): Promise<void> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, milliseconds);
  });
};

//// B )

const countSeconds = async (): Promise<void> => {
  for (let i = 0; i <= 10; i++) {
    console.log("B)", i);
    await waitFor(1000);
  }
};

countSeconds();

// EXTRA this was my first solution and ansver to extra question it indeed did not work as intended as timeout wasnt working as intended on each second and was going way faster)

// const waitFor = async (milliseconds: number): Promise<void> => {
//   return new Promise((resolve) => {
//     setTimeout(() => {
//       console.log(milliseconds / 100);
//       resolve();
//     }, milliseconds);
//   });
// };

// // waitFor(5000);

// const countSeconds = async (): Promise<void> => {
//   for (let i = 0; i <= 10; i++) {
//     await waitFor(100 * i);
//   }
//   console.log("All done");
// };

// countSeconds();
