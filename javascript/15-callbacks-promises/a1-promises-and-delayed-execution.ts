const sum = (limit: number) => {
  let sum = 0;
  for (let i = 0; i <= limit; i++) {
    sum += i;
  }
  console.log(sum);
};

sum(10);
new Promise((resolve) => {
  console.log("B) + C) Promise started!");
  setTimeout(() => {
    sum(50000);
    resolve(console.log("B) + C) Promise resolved!"));
  }, 2000);
});

// D)

const createDelayedCalculation = (limit: number, milliseconds: number) => {
  return new Promise((resolve) => {
    console.log("D) Promise started!");
    setTimeout(() => {
      sum(limit);
      resolve("D) Promise resolved!");
    }, milliseconds);
  });
};
createDelayedCalculation(20000000, 2000).then((result) => console.log(result));

createDelayedCalculation(50000, 500).then((result) => console.log(result));

// E)

// Reason why 2nd log show up first is because asynch javascript continue reading the code while waiting for the promise to resolve.

// and the time it takes to finnish the first promise is longer than the time it takes to finnish the second promise. So the second promise is resolved first.
