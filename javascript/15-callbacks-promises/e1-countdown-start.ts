let timer = function (time: number, callback: Function) {
  setTimeout(() => {
    callback();
  }, time);
};

timer(1000, () => {
  console.log("3 ⇒ Wait 1 second");
  timer(1000, () => {
    console.log("..2  ⇒ Wait another second");
    timer(1000, () => {
      console.log("….1 ⇒ Wait the last 1 second..");
      timer(1000, () => {
        console.log("GOO...");
      });
    });
  });
});
