{
  let timer = function (time: number, callback: Function) {
    setTimeout(() => {
      callback();
    }, time);
  };

  new Promise<void>((resolve) => {
    timer(1000, () => {
      console.log("3..");
      resolve();
    });
  })
    .then(() => {
      return new Promise<void>((resolve) => {
        timer(1000, () => {
          console.log("2..");
          resolve();
        });
      });
    })
    .then(() => {
      return new Promise<void>((resolve) => {
        timer(1000, () => {
          console.log("1..");
          resolve();
        });
      });
    })
    .then(() => {
      return new Promise<void>((resolve) => {
        timer(1000, () => {
          console.log("GO!..");
          resolve();
        });
      });
    });
}
