{
  class Shape {
    width: number;
    height: number;
    constructor(width: number, height: number) {
      this.width = width;
      this.height = height;
    }
    getArea() {
      return 0;
    }
  }

  class Rectangle extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return this.width * this.height;
    }
  }

  class Ellipse extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return Math.PI * (this.width / 2) * (this.height / 2);
    }
  }

  class Triangle extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return (this.width * this.height) / 2;
    }
  }

  const newRectangle = new Rectangle(10, 20);
  const newEllipse = new Ellipse(10, 20);
  const newTriangle = new Triangle(10, 20);

  console.log(newRectangle.getArea());
  console.log(newEllipse.getArea());
  console.log(newTriangle.getArea());
}
