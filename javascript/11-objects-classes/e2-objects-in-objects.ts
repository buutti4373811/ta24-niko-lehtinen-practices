interface Student {
  name: string;
  credits: number;
  courseGrades: {
    [course: string]: number;
  };
}

const student: Student = {
  name: "Aili",
  credits: 45,
  courseGrades: {
    "Intro to Programming": 4,
    "JavaScript Basics": 3,
    "Functional Programming": 5,
  },
};

console.log("A)", student.name);

student.courseGrades["Program Design"] = 3;

console.log("B)", student);

student.courseGrades["JavaScript Basics"] = 4;

console.log("C)", student);
