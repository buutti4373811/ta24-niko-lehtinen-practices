{
  const getCountOfLetters = (str: string): any => {
    const countOfLetters: { [key: string]: number } = {};

    for (let i = 0; i < str.length; i++) {
      const char = str[i].toLowerCase();
      if (char >= "a" && char <= "z") {
        if (countOfLetters[char]) {
          countOfLetters[char]++;
        } else {
          countOfLetters[char] = 1;
        }
      }
    }

    return countOfLetters;
  };

  const result = getCountOfLetters("a black cat ");
  console.log(result);
}
