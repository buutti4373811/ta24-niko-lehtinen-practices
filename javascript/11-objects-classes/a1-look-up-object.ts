interface LookUp {
  [key: string]: number;
}

//prettier-ignore
const lookUp:LookUp = {
  "S": 8,
  "A": 6,
  "B": 4,
  "C": 3,
  "D": 2,
  "F": 0,
};
//A)
const calculateTotalScore = (string: string) => {
  let total = 0;

  for (let i = 0; i < string.length; i++) {
    total = total + lookUp[string[i]];
  }
  return total;
};

const totalScore = calculateTotalScore("DFCBDABSB");
console.log("A)", totalScore);

//B)

const calculateAverageScore = (string: string) => {
  let total = 0;

  for (let i = 0; i < string.length; i++) {
    total = total + lookUp[string[i]];
  }
  return total / string.length;
};

const averageScore = calculateAverageScore("DFCBDABSB");
console.log("B)", averageScore);
//C)

const gradesArray = ["AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC"];

const avgArray = gradesArray.map((grades) => {
  return calculateAverageScore(grades);
});

console.log("C)", avgArray);
