// A)

interface Translations {
  [key: string]: string;
}

const translations = {
  hello: "hei",
  world: "maailma",
  bit: "bitti",
  byte: "tavu",
  integer: "kokonaisluku",
  boolean: "totuusarvo",
  string: "merkkijono",
  network: "verkko",
};

// B)

const printTranslatableWords = () => {
  console.log(Object.keys(translations));
};
printTranslatableWords();

// C)

const translate = (word: string) => {
  if (!translations[word as keyof typeof translations]) {
    console.log("Word not found in dictionary");
    return null;
  }
  return translations[word as keyof typeof translations];
};
console.log(translate("network"));
