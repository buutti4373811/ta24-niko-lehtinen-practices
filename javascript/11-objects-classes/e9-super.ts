class Robot {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
  handleMessage(message: string) {
    if (message === "moveNorth") {
      this.y++;
    } else if (message === "moveSouth") {
      this.x++;
    }
  }
}

class FlexibleRobot extends Robot {
  constructor(x: number, y: number) {
    super(x, y);
  }
  handleMessage(message: string) {
    if (message === "moveNE") {
      this.x++;
    } else if (message === "moveNW") {
      this.y++;
    }

    super.handleMessage(message);
  }
}
// not sure if naming is correct, but it works change the names of x and y to make it more clear
const robot = new FlexibleRobot(0, 0);
robot.handleMessage("moveNE");
robot.handleMessage("moveNE");
robot.handleMessage("moveNorth");
robot.handleMessage("moveNW");
robot.handleMessage("moveSouth");
robot.handleMessage("moveSouth");
console.log(robot);
