{
  let x = 0;
  let y = 0;
  let continueExecution = true;

  const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

  //prettier-ignore
  const commandHandler = {
      "N": () => y++,
      "E": () => x++,
      "S": () => y--,
      "W": () => x--,
      "C": () => {},
      "B": () => {continueExecution = false;},
    };

  const commandListFunction = (commandList: string) => {
    for (let i = 0; i < commandList.length && continueExecution; i++) {
      const command = commandList.charAt(i);
      switch (command) {
        case "N":
          commandHandler[command]();
          break;
        case "E":
          commandHandler[command]();
          break;
        case "S":
          commandHandler[command]();
          break;
        case "W":
          commandHandler[command]();
          break;
        case "C":
          break;
        case "B":
          commandHandler[command]();
          break;
      }
    }
  };

  commandListFunction(commandList);
  console.log(x, y);
}
