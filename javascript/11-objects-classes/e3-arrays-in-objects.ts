{
  interface Course {
    name: string;
    grade: number;
  }
  interface Student {
    name: string;
    credits: number;
    courseGrades: { [key: string]: number } | Course[];
  }

  const student: Student = {
    name: "Aili",
    credits: 45,
    courseGrades: {
      "Intro to Programming": 4,
      "JavaScript Basics": 3,
      "Functional Programming": 5,
    },
  };

  student.courseGrades = Object.entries(student.courseGrades).map(
    ([name, grade]) => ({
      name,
      grade,
    })
  );

  console.log("A)" + student.courseGrades);

  console.log(
    "B)",
    student.name +
      " got " +
      student.courseGrades[0].grade +
      " from " +
      student.courseGrades[0].name
  );

  const addCourse = (courseName: string, grade: number): void => {
    (student.courseGrades as Course[]).push({ name: courseName, grade: grade });
  };

  addCourse("Program Design", 3);
  console.log("C)", student);
}
