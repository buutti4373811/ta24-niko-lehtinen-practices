{
  class Rectangle {
    width: number;
    height: number;

    constructor(width: number, height: number) {
      this.width = width;
      this.height = height;
    }
    getArea() {
      return this.width * this.height;
    }
  }

  const newRectangle = new Rectangle(10, 20);

  console.log(newRectangle.getArea());
}
