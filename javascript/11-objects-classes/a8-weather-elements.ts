class WeatherEvent {
  timeStamp: string;
  constructor(timeStamp: string) {
    this.timeStamp = timeStamp;
  }
  getInformation() {
    return "";
  }
  print() {
    console.log(this.timeStamp, this.getInformation());
  }
}

const weather1 = new WeatherEvent("12:00");
weather1.print();
console.log("A)", weather1);

class TemperatureChangeEvent extends WeatherEvent {
  temperature: number;

  constructor(timeStamp: string, temperature: number) {
    super(timeStamp);
    this.temperature = temperature;
  }

  getInformation() {
    return "temperature: your temperature value °C" + this.temperature;
  }
}
const temperature1 = new TemperatureChangeEvent("12:00", 33);
console.log("B)", temperature1.getInformation());

class humidityChangeEvent extends WeatherEvent {
  humidity: number;

  constructor(timeStamp: string, humidity: number) {
    super(timeStamp);
    this.humidity = humidity;
  }

  getInformation() {
    return "humidity: " + this.humidity + "%";
  }
}

const humidity1 = new humidityChangeEvent("12:00", 50);
console.log("C)", humidity1.getInformation());

class WindStrength extends WeatherEvent {
  windStrength: number;

  constructor(timeStamp: string, windStrength: number) {
    super(timeStamp);
    this.windStrength = windStrength;
  }

  getInformation() {
    return "wind strength: " + this.windStrength + "m/s";
  }
}

const currentDate = new Date();
const year = currentDate.getFullYear();
const month = String(currentDate.getMonth() + 1).padStart(2, "0"); // Adding 1 as months are zero-based
const day = String(currentDate.getDate()).padStart(2, "0");
const hours = String(currentDate.getHours()).padStart(2, "0");
const minutes = String(currentDate.getMinutes()).padStart(2, "0");

const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;

const wind1 = new WindStrength("12:00", 5);
console.log("D)", wind1.getInformation());

const weatherEvents: WeatherEvent[] = [];

weatherEvents.push(new TemperatureChangeEvent(formattedDate, 33));
weatherEvents.push(new humidityChangeEvent(formattedDate, 95));
weatherEvents.push(new WindStrength(formattedDate, 2.2));

weatherEvents.forEach((weatherEvent) => weatherEvent.print());
