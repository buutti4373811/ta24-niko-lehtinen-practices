{
  class Shape {
    width: number;
    height: number;
    constructor(width: number, height: number) {
      this.width = width;
      this.height = height;
    }
    getArea() {
      return 0;
    }
  }

  class Rectangle extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return this.width * this.height;
    }
  }

  class Square extends Rectangle {
    constructor(width: number) {
      super(width, width);
    }
  }

  class Ellipse extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return Math.PI * (this.width / 2) * (this.height / 2);
    }
  }
  class Circle extends Ellipse {
    constructor(radius: number) {
      super(radius * 2, radius * 2);
    }
  }

  class Triangle extends Shape {
    constructor(width: number, height: number) {
      super(width, height);
      this.width = width;
      this.height = height;
    }
    getArea() {
      return (this.width * this.height) / 2;
    }
  }

  const newSquare = new Square(10);
  const newCircle = new Circle(10);
  console.log("A)" + newSquare.getArea());
  console.log("B)" + newCircle.getArea());
}
