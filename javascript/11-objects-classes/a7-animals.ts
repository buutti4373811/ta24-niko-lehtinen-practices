class Animal {
  weight: number;
  cuteness: number;
  constructor(weight: number, cuteness: number) {
    this.weight = weight;
    this.cuteness = cuteness;
  }
  makeSound(): void {
    console.log("silence");
  }
}
const animal = new Animal(10, 5);
console.log("A)", animal);
animal.makeSound();
console.log("A)", animal);

class Cat extends Animal {
  constructor(weight: number, cuteness: number) {
    super(weight, cuteness);
  }
  makeSound(): void {
    console.log("meow");
  }
}

const cat = new Cat(5, 10);
console.log("B)", cat);
cat.makeSound();
console.log("B)", cat);

class Dog extends Animal {
  breed: string;
  constructor(weight: number, cuteness: number, breed: string) {
    super(weight, cuteness);

    this.breed = breed;
  }
  makeSound(): void {
    if (this.cuteness > 4) {
      console.log("woof");
    } else {
      console.log("bark");
    }
  }
}

const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
console.log("C)", dog1);
console.log("C)", dog2);
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"
