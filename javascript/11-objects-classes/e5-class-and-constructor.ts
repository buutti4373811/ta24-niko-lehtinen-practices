class Rectangle {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }
}

const rectangle1 = new Rectangle(10, 20);
const rectangle2 = new Rectangle(20, 40);
const rectangle3 = new Rectangle(40, 50);

console.log(rectangle1, rectangle2, rectangle3);
