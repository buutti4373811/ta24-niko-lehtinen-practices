interface Fruits {
  banana: number;
  apple: number;
  mango: number;
  lemon: number;
}
const fruits: Fruits = {
  banana: 118,
  apple: 85,
  mango: 200,
  lemon: 65,
};

const printWeight = (fruit: string) => {
  if (!(fruit in fruits)) {
    console.log(
      "Sorry, we don't have the weight for " +
        fruit +
        ". supported fruits are: " +
        Object.keys(fruits).join(", ") +
        "."
    );
    return;
  }
  const weight = fruits[fruit as keyof Fruits];
  console.log(fruit + " weighs " + weight + " grams.");
};

printWeight("apple");
printWeight("bananaa");
