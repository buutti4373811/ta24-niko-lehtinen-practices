class Room {
  width: number;
  height: number;
  furniture?: string[];
  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
    this.furniture = [];
  }

  getArea() {
    return this.width * this.height;
  }
  addFurniture(furniture: string) {
    this.furniture?.push(furniture);
  }
}

const room = new Room(10, 20);
console.log("A)", room);

const area = room.getArea();
console.log("B)", area);

room.addFurniture("bed");
room.addFurniture("table");
console.log("C)", room);
