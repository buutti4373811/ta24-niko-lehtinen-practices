class Robot {
  x = 0;
  y = 0;
  continueExecution = true;

  commandHandler = {
    N: () => this.y++,
    E: () => this.x++,
    S: () => this.y--,
    W: () => this.x--,
    C: () => {},
    B: () => {
      this.continueExecution = false;
    },
  };

  handleCommandList(commandList: string) {
    for (let i = 0; i < commandList.length && this.continueExecution; i++) {
      const command = commandList.charAt(i);
      this.commandHandler[command as keyof typeof this.commandHandler]();
    }
  }
}
const robot = new Robot();
console.log(robot);
robot.handleCommandList("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
console.log(robot);
