import axios from "axios";

const getPostComments = async (id: number) => {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/posts/${id}/comments`
  );
  console.log(response.data);
};

getPostComments(5);
