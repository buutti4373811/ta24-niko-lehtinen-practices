import axios from "axios";

interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: {
    rate: number;
    count: number;
  };
}
// A)
const getFakeStoreProducts = async () => {
  const res = await axios("https://fakestoreapi.com/products");
  const products: Product[] = res.data as Product[];
  products.map((product: Product) => {
    console.log("A)", product.title);
  });
};
getFakeStoreProducts();

// B

const addFakeStoreProduct = async () => {
  const newProduct = {
    title: "test product",
    price: 13.5,
    description: "lorem ipsum set",
    image: "https://i.pravatar.cc",
    category: "electronic",
  };

  const res = await axios.post("https://fakestoreapi.com/products", newProduct);

  console.log("B)", res.data.id);
};

addFakeStoreProduct();

// C

const deleteFakeStoreProduct = async (id: number) => {
  const res = await axios.delete(`https://fakestoreapi.com/products/${id}`);
  console.log("C)", res.data.title);
};

deleteFakeStoreProduct(1);
