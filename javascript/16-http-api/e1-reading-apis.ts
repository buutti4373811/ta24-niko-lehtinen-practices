import axios from "axios";

export default async function getPostById(id: number) {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );

  const user = response.data.userId;
  const userResponse = await axios.get(
    `https://jsonplaceholder.typicode.com/users/${user}`
  );

  console.log(
    `E1- Post #[${response.data.id}] by: [${userResponse.data.username} ] [${response.data.title}]).)`
  );
  return response.data;
}

getPostById(5);
