import axios from "axios";
import getPostById from "./e1-reading-apis";

// A )
const sendNewPost = async (post: any) => {
  const response = await axios.post(
    `https://jsonplaceholder.typicode.com/posts`,
    JSON.stringify(post)
  );
  console.log("A) Status code for creating new post:", response.status);
  console.log("A)", response.data);
};
sendNewPost({ title: "foo", body: "bar", userId: 1 });

// B (im not sure if i understood this correctly))

const updateTitle = async (title: any) => {
  const { id } = await getPostById(5);
  const response = await axios.put(
    `https://jsonplaceholder.typicode.com/posts/${id}`,
    JSON.stringify({ title })
  );
  console.log("B) Status code for updating post title:", response.status);
  console.log("B)", response.data);
};

updateTitle("Hello, World!");
