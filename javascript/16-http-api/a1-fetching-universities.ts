import axios from "axios";

const getUbuntuUniversities = async () => {
  const response = await axios.get(
    `http://universities.hipolabs.com/search?country=Finland`
  );

  return response.data;
};

getUbuntuUniversities().then((data) => {
  const universityArray = data.map((university: any) => {
    return university.name;
  });

  console.log(universityArray);
});
