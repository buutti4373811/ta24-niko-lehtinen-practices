{
  let x = 0;
  let y = 0;
  const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

  for (let i = 0; i < commandList.length; i++) {
    const command = commandList[i];

    if (command === "N") {
      y++;
    } else if (command === "E") {
      x++;
    } else if (command === "S") {
      y--;
    } else if (command === "W") {
      x--;
    } else if (command === "C") {
      continue;
    } else if (command === "B") {
      break;
    }
  }

  console.log("Final X coordinate:", x);
  console.log("Final Y coordinate:", y);
}
