// i had to wrap this into object to remove the global scope of num variable
// to avoid conflict with next exercise scope

{
  let num = 3;

  while (num <= 15) {
    console.log(num);
    num += 3;
  }
}
