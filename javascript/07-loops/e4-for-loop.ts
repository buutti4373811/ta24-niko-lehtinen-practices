for (let i = 0; i <= 1000; i += 100) {
  console.log(i);
}
for (let i = 1; i <= 128; i *= 2) {
  console.log(i);
}

for (let i = 3; i <= 16; i += 3) {
  console.log(i);
}
for (let i = 9; i >= 0; i--) {
  console.log(i);
}
for (let i = 1; i <= 4; i++) {
  console.log(i);
  console.log(i);
  console.log(i);
}

// Nested loop was my solution to this problem otherwiaw same could be archived with single loop and mathematically calculated value
for (let i = 0; i < 3; i++) {
  for (let j = 0; j <= 4; j++) {
    console.log(j);
  }
}
