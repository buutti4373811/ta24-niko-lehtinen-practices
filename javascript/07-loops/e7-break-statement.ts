let factorial = 1;
let number = 1;

while (true) {
  factorial *= number;

  if (factorial % 600 === 0) {
    console.log(
      "The smallest number with a factorial divisible by 600 is:",
      number
    );
    break;
  }

  number++;
}
