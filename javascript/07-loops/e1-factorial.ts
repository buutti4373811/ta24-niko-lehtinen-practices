// again i have to wrap this into function otherwise i get typescript error for having scope of variables in another file

{
  let n = 4;
  let factorial = 1;
  let i = 1;

  while (i <= n) {
    factorial *= i;
    i++;
  }
  console.log(factorial);
}
