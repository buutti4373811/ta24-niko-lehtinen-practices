const agesArray = [20, 35, 27, 44, 32];

console.log(agesArray);

function calculateAverage(ages: number[]) {
  let sum = 0;
  for (let i = 0; i < ages.length; i++) {
    sum += ages[i];
  }
  console.log(sum / ages.length);
}

calculateAverage(agesArray);
