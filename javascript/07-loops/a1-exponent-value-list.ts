function exponentValueList(n: number, raise: number) {
  let result = 1;
  if (n <= 0) {
    console.log("n needs to be a positive number");
  }
  for (let i = 1; i <= n; i++) {
    result *= raise;
    console.log(result);
  }
}

exponentValueList(4, 2);
exponentValueList(0, 2);
exponentValueList(-4, 2);
exponentValueList(10, 4);
