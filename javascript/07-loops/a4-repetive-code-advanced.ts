// i did not perfectly do this had to look at the solution to get the idea of the task
function getLetterCount(string: string, letter: string) {
  let letterCount = 0;

  for (let i = 0; i < string.length; i++) {
    if (string.charAt(i).toLowerCase() === letter) {
      letterCount++;
    }
  }

  return letterCount;
}

function checkSentenceVowels(sentence: string) {
  const countOfAs = getLetterCount(sentence, "a");
  const countOfEs = getLetterCount(sentence, "e");
  const countOfIs = getLetterCount(sentence, "i");
  const countOfOs = getLetterCount(sentence, "o");
  const countOfUs = getLetterCount(sentence, "u");
  const countOfYs = getLetterCount(sentence, "y");

  console.log("A letter count: " + countOfAs);
  console.log("E letter count: " + countOfEs);
  console.log("I letter count: " + countOfIs);
  console.log("O letter count: " + countOfOs);
  console.log("U letter count: " + countOfUs);
  console.log("Y letter count: " + countOfYs);

  const totalCount =
    countOfAs + countOfEs + countOfIs + countOfOs + countOfUs + countOfYs;

  console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
