const fibonacci = (n: number): number => {
  if (n <= 1) {
    return n;
  }
  const result = fibonacci(n - 1) + fibonacci(n - 2);
  console.log(result);
  return result;
};

fibonacci(10);
