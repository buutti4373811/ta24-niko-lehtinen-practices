const calcF = (n: number): number => {
  if (n === 1) {
    return 1;
  } else if (n === 0) {
    return 0;
  }
  return calcF(n - 2) * 3 + calcF(n - 1);
};

console.log(calcF(17));
