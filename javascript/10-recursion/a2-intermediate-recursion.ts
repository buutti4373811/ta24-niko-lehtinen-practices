const wordArray = ["The", "quick", "silver", "wolf"];

const sentencify = (words: string[], index: number): any => {
  if (index === words.length - 1) {
    return words[index];
  }
  return words[index] + " " + sentencify(words, index + 1);
};

console.log(sentencify(wordArray, 0));
