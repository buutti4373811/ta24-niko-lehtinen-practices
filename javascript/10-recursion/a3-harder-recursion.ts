function mergeSort(arr: number[]): number[] {
  if (arr.length <= 1) {
    return arr;
  }

  // Splitting the array into two halves
  const mid = Math.floor(arr.length / 2);
  const leftHalf = arr.slice(0, mid);
  const rightHalf = arr.slice(mid);

  // Recursive call to sort each half
  const sortedLeftHalf = mergeSort(leftHalf);
  const sortedRightHalf = mergeSort(rightHalf);

  // Merging the sorted halves
  return merge(sortedLeftHalf, sortedRightHalf);
}

function merge(left: number[], right: number[]): number[] {
  let merged: any[] = [];
  let leftIndex = 0;
  let rightIndex = 0;

  // Merge until one of the arrays is exhausted
  while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex] < right[rightIndex]) {
      merged.push(left[leftIndex]);
      leftIndex++;
    } else {
      merged.push(right[rightIndex]);
      rightIndex++;
    }
  }

  // Append remaining elements from the non-empty array
  merged = merged.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
  return merged;
}

// Example usage:
const arr = [4, 19, 7, 1, 9, 22, 6, 13];
const sortedArr = mergeSort(arr);
console.log("Sorted array:", sortedArr);
