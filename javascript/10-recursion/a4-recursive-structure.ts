interface ObjectInterface {
  name: string;
  width: number;
  height: number;
  children: ObjectInterface[];
}
function buildUserInterface() {
  const mainWindow: ObjectInterface = {
    name: "MainWindow",
    width: 600,
    height: 400,
    children: [],
  };
  const buttonExit: ObjectInterface = {
    name: "ButtonExit",
    width: 100,
    height: 30,
    children: [],
  };
  mainWindow.children.push(buttonExit);

  const settingsWindow: ObjectInterface = {
    name: "SettingsWindow",
    width: 400,
    height: 300,
    children: [],
  };
  const buttonReturnToMenu: ObjectInterface = {
    name: "ButtonReturnToMenu",
    width: 100,
    height: 30,
    children: [],
  };
  settingsWindow.children.push(buttonReturnToMenu);
  mainWindow.children.push(settingsWindow);

  const profileWindow: ObjectInterface = {
    name: "ProfileWindow",
    width: 500,
    height: 400,
    children: [],
  };
  const profileInfoPanel: ObjectInterface = {
    name: "ProfileInfoPanel",
    width: 200,
    height: 200,
    children: [],
  };
  profileWindow.children.push(profileInfoPanel);
  mainWindow.children.push(profileWindow);

  return mainWindow;
}

const userInterfaceTree = buildUserInterface();

console.log("a)", userInterfaceTree);

const findControl = (
  control: ObjectInterface,
  name: string
): ObjectInterface | null => {
  if (control.name === name) {
    return control;
  }
  for (let i = 0; i < control.children.length; i++) {
    const result = findControl(control.children[i], name);
    if (result) {
      return result;
    }
  }
  return null;
};

const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
profileInfoPanel!.width += 100;
console.log("B)", profileInfoPanel);
