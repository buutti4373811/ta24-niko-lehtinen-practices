// this was too long to find motivation to do it, kinda skipped it

let x = 0;
let y = 0;

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

const letterToNumberMap = { N: 0, E: 1, S: 2, W: 3, C: 4, B: 5 };

const commandNumberArray: number[] = [];

for (let i = 0; i < commandList.length; i++) {
  const commandNumber: number =
    letterToNumberMap[commandList.charAt(i) as keyof typeof letterToNumberMap];
  commandNumberArray.push(commandNumber);
}

const functionArray = [() => y++, () => x++, () => y--, () => x--, () => {}];

const stopProcessingCommandIndex = letterToNumberMap.B;

for (let i = 0; i < commandNumberArray.length; i++) {
  const commandNumber = commandNumberArray[i];

  if (commandNumber === stopProcessingCommandIndex) {
    break;
  }

  const func = functionArray[commandNumber];
  func();
}

console.log(`Final robot position, x: ${x}, y: ${y}`);
