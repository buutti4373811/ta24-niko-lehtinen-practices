const arr = [1, 3, 4, 7, 11];

const insertNumber = (arr: number[], number: number) => {
  if (number > arr[arr.length - 1]) {
    arr.push(number);
    console.log(arr);
    return;
  }

  for (let i = 0; i < arr.length; i++) {
    if (number < arr[i] && number > arr[i - 1]) {
      arr.splice(i, 0, number);
      console.log(arr);
      return;
    }
  }
};
insertNumber(arr, 90);
