const strings = ["the", "quick", "brown", "fox"];

console.log(strings);

console.log(strings[1], strings[2]);

strings[2] = "gray";

console.log(strings[2]);

strings.push("over", "lazy", "dog");
strings.unshift("pangram");

console.log(strings);

strings.splice(5, 0, "jump");
strings.splice(7, 0, "the");

console.log(strings);
strings.shift();
console.log(strings);
strings.splice(4, 5);
strings.splice(2, 1);
console.log(strings);
