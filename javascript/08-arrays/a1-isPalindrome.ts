function isPalindrome(str: string): boolean {
  let lastIndex = str.length - 1;
  for (let i = 0; i < str.length / 2; i++) {
    if (str[i] != str[lastIndex]) {
      return false;
    }
    lastIndex--;
  }
  return true;
}

console.log(isPalindrome("madam"));
console.log(isPalindrome("juusto"));
