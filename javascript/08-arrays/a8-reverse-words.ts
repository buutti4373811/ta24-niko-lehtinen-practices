function reverseWords(sentence: string): string {
  const words = sentence.split(" ");
  let reversedSentence = "";

  for (let wordIndex = 0; wordIndex < words.length; wordIndex++) {
    const word = words[wordIndex];

    for (let letterIndex = word.length - 1; letterIndex >= 0; letterIndex--) {
      reversedSentence += word.charAt(letterIndex);
    }

    reversedSentence += " ";
  }

  reversedSentence.trimEnd();

  return reversedSentence;
}
console.log(reverseWords("hello"));
