const fruits: string[] = [
  "cherry",
  "banana",
  "coconut",
  "apple",
  "pear",
  "pineapple",
  "lemon",
  "pumpkin",
];

const empty: string[] = [];

for (let fruit of fruits) {
  if (fruit.length > 6) {
    empty.push(fruit);
  }
}

console.log(empty);
