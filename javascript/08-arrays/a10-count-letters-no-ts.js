const charIndex = {
  a: 0,
  b: 1,
  c: 2,
  d: 3,
  e: 4,
  f: 5,
  g: 6,
  h: 7,
  i: 8,
  j: 9,
  k: 10,
  l: 11,
  m: 12,
  n: 13,
  o: 14,
  p: 15,
  q: 16,
  r: 17,
  s: 18,
  t: 19,
  u: 20,
  v: 21,
  w: 22,
  x: 23,
  y: 24,
  z: 25,
};

const getCountOfLetters = (str) => {
  // make an new array of 26 zeros and give them default value of 0
  const counts = [];
  for (let i = 0; i < 26; i++) {
    counts[i] = 0;
  }

  for (let i = 0; i < str.length; i++) {
    const char = str[i].toLowerCase();
    // if check that char is a letter from a to z
    if (char >= "a" && char <= "z") {
      // use char as a key to get the value from charIndex object

      //finally use the value from charIndex object to get the index of counts array and increment the value by one
      counts[charIndex[char]]++;
    }
  }

  return counts;
};

const result = getCountOfLetters("a black cat");

console.log(result);
