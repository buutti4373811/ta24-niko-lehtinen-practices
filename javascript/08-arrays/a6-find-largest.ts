const array = [4, 19, 7, 1, 9, 22, 6, 13];
const findLargest = (arr: number[]): number => {
  let largest = arr[0];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] > largest) {
      largest = arr[i];
    }
  }
  console.log(largest);
  return largest;
};

findLargest(array);
