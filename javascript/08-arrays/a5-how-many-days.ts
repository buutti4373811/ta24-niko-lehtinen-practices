function daysInMonth(month: number) {
  const daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  const monthIndex = month - 1;
  return daysInMonths[monthIndex];
}

console.log(daysInMonth(11));
