const numberRange = (start: number, end: number): number[] => {
  const result: number[] = [];
  if (end > start) {
    for (let i = start; i <= end; i++) {
      result.push(i);
    }
  } else if (start > end) {
    for (let i = start; i >= end; i--) {
      result.push(i);
    }
  } else {
    console.log(
      "start and end are equal, no range to create.here is the number in an array tho"
    );
    return [start];
  }

  return result;
};
console.log(numberRange(5, 10));
console.log(numberRange(15, 10));
console.log(numberRange(5, 5));
