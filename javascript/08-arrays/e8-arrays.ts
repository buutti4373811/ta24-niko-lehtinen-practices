const numArray = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (const index in numArray)
  if (numArray[index] % 3 === 0) console.log(numArray[index]);

for (const number of numArray) if (number % 3 === 0) console.log(number);
