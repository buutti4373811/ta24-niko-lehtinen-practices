const fruits = ["banana", "apple", "grapefruit", "pear", "pineapple", "lemon"];

const deleteFruit = (fruits: string[], fruitToDelete: string): string[] => {
  const index = fruits.indexOf(fruitToDelete);

  fruits.splice(index, 1);

  console.log(fruits);
  return fruits;
};

deleteFruit(fruits, "apple");
