const numberArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const stringArr = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];

const isArray = (arr: unknown[]): boolean => {
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] !== "number") {
      return false;
    }
  }
  return true;
};

console.log(isArray(numberArr));
console.log(isArray(stringArr));
