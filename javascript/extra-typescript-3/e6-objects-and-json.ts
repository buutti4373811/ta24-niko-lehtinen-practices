import fs from "fs";

const library = [
  {
    author: "David Wallace",
    title: "Infinite Jest",
    readingStatus: false,
    id: 1,
  },
  {
    author: "Douglas Hofstadter",
    title: "Gödel, Escher, Bach",
    readingStatus: true,
    id: 2,
  },
  {
    author: "Harper Lee",
    title: "To Kill A Mockingbird",
    readingStatus: false,
    id: 3,
  },
];

const getBook = (id: number) => library.find((book) => book.id === id);

const printBookData = (id: number) =>
  console.log(library.find((book) => book.id === id));

const printReadingStatus = (author: string, title: string) => {
  const book = library.find(
    (book) => book.author === author && book.title === title
  );

  console.log(book?.readingStatus);
};

const addNewBook = (author: string, title: string) => {
  const newBook = {
    author,
    title,
    readingStatus: false,
    id: library.length + 1,
  };

  library.push(newBook);
};
const readBook = (id: number) => {
  const book = library.find((book) => book.id === id);
  if (book?.readingStatus === false) {
    book.readingStatus = true;
  }
};

const saveToJSON = () => {
  fs.writeFileSync("library.json", JSON.stringify(library), "utf8");
};

const loadFromJSON = () => {
  const library_string = fs.readFileSync("library.json", "utf8");
  const library = JSON.parse(library_string);
  console.log("load from json", library);
};

printBookData(2);
console.log(getBook(2));
printReadingStatus("Douglas Hofstadter", "Gödel, Escher, Bach");
addNewBook("J.K. Rowling", "Harry Potter");
console.log(library);
readBook(4);
console.log(library);
saveToJSON();
loadFromJSON();
