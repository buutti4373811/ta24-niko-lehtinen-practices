// Original code
// const tree = { x: 6, y: 7, hitpoints: 30 };
// const rock = { x: 3, y: 11, hitpoints: 90 };
// const damage = 15;

// {
//   let treeHitpointsLeft;
//   let rockHitpointsLeft;

//   const hitpointsRock = rock.hitpoints;
//   const hitpointsTree = tree.hitpoints;
//   rockHitpointsLeft = hitpointsRock - damage;
//   console.log("Rock hitpoints left: " + rockHitpointsLeft);

//   treeHitpointsLeft = hitpointsTree - damage;

//   console.log("Tree hitpoints left: " + treeHitpointsLeft);
// }

//Refactored code with functions and typescript
const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y: 11, hitpoints: 90 };
const damageTaken = 15;

const damageTree = (dmg: number) => {
  let treeHitpointsLeft;
  treeHitpointsLeft = tree.hitpoints - dmg;
  console.log("1. Tree hitpoints left: " + treeHitpointsLeft);
};
damageTree(damageTaken);
const damageRock = (dmg: number) => {
  let rockHitpointsLeft;
  rockHitpointsLeft = rock.hitpoints - dmg;
  console.log("1.Rock hitpoints left: " + rockHitpointsLeft);
};
damageRock(damageTaken);

const damage = (
  hitTarget: { x: number; y: number; hitpoints: number },
  dmg: number
) => {
  let hitpointsLeft;
  hitpointsLeft = hitTarget.hitpoints - dmg;
  console.log("2. Hitpoints left: " + hitpointsLeft);
};
damage(tree, damageTaken);
damage(rock, damageTaken);
