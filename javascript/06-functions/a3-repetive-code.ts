const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

// original code
// let firstTriangleArea = firstTriangle.width * firstTriangle.length;
// firstTriangleArea = firstTriangleArea / 2.0;

// let secondTriangleArea = secondTriangle.width * secondTriangle.length;
// secondTriangleArea = secondTriangleArea / 2.0;

// let thirdTriangleArea = thirdTriangle.width * thirdTriangle.length;
// thirdTriangleArea = thirdTriangleArea / 2.0;

// console.log("Area of first triangle: " + firstTriangleArea);
// console.log("Area of second triangle: " + secondTriangleArea);
// console.log("Area of third triangle: " + thirdTriangleArea);

// refactored code
const triangleArea = (triangle: { width: number; length: number }) => {
  let area = triangle.width * triangle.length;
  return area / 2.0;
};

console.log("Area of first triangle: " + triangleArea(firstTriangle));
console.log("Area of second triangle: " + triangleArea(secondTriangle));
console.log("Area of third triangle: " + triangleArea(thirdTriangle));

// extra

const biggestArea = (
  triangle1Area: number,
  triangle2Area: number,
  triangle3Area: number
) => {
  if (triangle1Area > triangle2Area && triangle1Area > triangle3Area) {
    return console.log("First triangle has the biggest area: " + triangle1Area);
  } else if (triangle2Area > triangle1Area && triangle2Area > triangle3Area) {
    return console.log(
      "Second triangle has the biggest area: " + triangle2Area
    );
  } else {
    return console.log(
      "Second triangle has the biggest area: " + triangle3Area
    );
  }
};

biggestArea(
  triangleArea(firstTriangle),
  triangleArea(secondTriangle),
  triangleArea(thirdTriangle)
);
