// exercise 4.1
const array: number[] = [1, 2, 3, 4, 5];
console.log(array);

const updateFirstElement = (arr: number[]) => {
  arr[0] = 100;
  console.log(arr);
  return arr;
};

updateFirstElement(array);

// exercise 4.2

const object: { name: string; age: number; favouriteColor?: string } = {
  name: "Niko",
  age: 24,
};
console.log(object);

const updateObject = (obj: {
  name: string;
  age: number;
  favouriteColor?: string;
}) => {
  obj.favouriteColor = "blue";
  console.log(obj);
  return obj;
};
updateObject(object);
