function sum(a, b) {
  return a * b;
}

const sum2 = function (a, b) {
  return a * b;
};

const sum3 = (a, b) => {
  return a * b;
};

console.log(sum(1, 2));
console.log(sum2(3, 2));
console.log(sum3(5, 2));
