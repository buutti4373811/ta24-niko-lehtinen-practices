const minimum = (a: number, b: number, c: number): number => {
  if (a <= b && a <= c) {
    return a;
  } else if (b <= a && b <= c) {
    return b;
  } else {
    return c;
  }
};

console.log(minimum(1, 2, 3));
console.log(minimum(3, 2, 1));
console.log(minimum(2, 3, 1));

// or is this the intended way to solve assignment as it said "DO NOT call console.log in the function itself, but instead call it AFTER calling your function."?
const minValue = minimum(5, 3, 8);
console.log(minValue);
