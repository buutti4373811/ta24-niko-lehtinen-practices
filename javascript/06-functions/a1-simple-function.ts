const hello = (language: string) => {
  if (language === "fi") {
    return "Hei";
  } else if (language === "en") {
    return "Hello";
  } else if (language === "es") {
    return "Hola";
  } else {
    return "Language not supported.";
  }
};

console.log(hello("en"));
console.log(hello("es"));
console.log(hello("fi"));
console.log(hello("not supported language"));
