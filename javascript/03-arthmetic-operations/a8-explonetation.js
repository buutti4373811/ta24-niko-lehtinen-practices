// i did cheat abit here with chatGPT because im not that great at math nor do i think its that important to be good at math in programming
// Initial incomes of two player
let player1Income = 1000;
let player2Income = 1500;

// a) Print out the difference between the players' income
let incomeDifferenceBefore = player2Income - player1Income;
console.log("Income Difference Before: " + incomeDifferenceBefore);

// b) Alter each income by exponentiating them with 0.9
let exponent = 0.9;
let player1AdjustedIncome = player1Income ** exponent;
let player2AdjustedIncome = player2Income ** exponent;

// Calculate and print the difference between the incomes after exponentiation
let incomeDifferenceAfter = player2AdjustedIncome - player1AdjustedIncome;
console.log("Income Difference After Exponentiation: " + incomeDifferenceAfter);
