const daysInYear = 365;
const hoursInDay = 24;
const minutesInHour = 60;
const secondsInMinute = 60;

// Calculate the total number of seconds in a year
console.log(daysInYear * hoursInDay * minutesInHour * secondsInMinute);
