interface Student {
  name: string;
  grade: string;
}

const students = [
  { name: "Sami", score: 24.75 },
  { name: "Heidi", score: 20.25 },
  { name: "Jyrki", score: 27.5 },
  { name: "Helinä", score: 26.0 },
  { name: "Maria", score: 17.0 },
  { name: "Yrjö", score: 14.5 },
];

const getGrades = (students: { name: string; score: number }[]) => {
  const grades = students.map((student) => {
    const { name, score } = student;
    if (student.score < 14) {
      return { name, grade: "grade 0" };
    } else if (score >= 14 && score <= 17) {
      return { name, grade: "grade 1" };
    } else if (score > 17 && score <= 20) {
      return { name, grade: "grade 2" };
    } else if (student.score > 20 && student.score <= 23) {
      return { name: name, grade: "grade 3" };
    } else if (student.score > 23 && student.score <= 26) {
      return { name: name, grade: "grade 4" };
    } else if (student.score > 26) {
      return { name: name, grade: "grade 5" };
    }
  });

  return grades as Student[];
};

const grades = getGrades(students);
console.log(grades);
