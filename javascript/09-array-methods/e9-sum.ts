const numbersArray: (string | number)[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "DD"];

const sum = numbersArray.reduce((acc, cur) => {
  if (!isNaN(Number(cur))) {
    return Number(acc) + Number(cur);
  }
  return acc;
}, 0);

console.log(sum);
