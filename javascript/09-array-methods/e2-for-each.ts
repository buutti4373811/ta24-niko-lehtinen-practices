const numbers = [5, 13, 2, 10, 8];

// a)
let product = 1;
numbers.forEach((number) => {
  product *= number;
});

console.log(product);
// b)
let sum = 0;
let i = 0;

numbers.forEach((number, index) => {
  sum += number;
  i = index + 1;
});

let avg = sum / i;
console.log(avg);
