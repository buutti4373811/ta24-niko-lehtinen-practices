{
  const arr = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

  const endsWithT: string | undefined = arr.find((animal) => {
    if (animal.endsWith("t")) {
      return animal;
    }
  });
  console.log(endsWithT);
}
