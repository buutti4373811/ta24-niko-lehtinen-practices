{
  const arr = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

  const findIndex = arr.findIndex((animal) => {
    if (animal.length > 5) {
      return animal;
    }
  });
  console.log(findIndex, arr[findIndex]);
}
