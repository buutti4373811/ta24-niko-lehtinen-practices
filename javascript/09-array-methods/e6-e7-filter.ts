{
  const arr = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

  const filteredArray = arr.filter((animal) => {
    if (animal.includes("o")) {
      return animal;
    }
  });
  console.log("exercise 6a below:");
  console.log(filteredArray);

  const filteredArray2 = arr.filter((animal) => {
    if (!animal.includes("o") && !animal.includes("h")) {
      return animal;
    }
  });
  console.log("exercise 6b below:");
  console.log(filteredArray2);

  console.log("exercise 7 below:");
  filteredArray.forEach((animal) => {
    console.log(animal);
  });
}
