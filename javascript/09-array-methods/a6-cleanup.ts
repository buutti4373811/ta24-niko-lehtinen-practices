const objectArray = [
  { x: 14, y: 21, type: "tree", toDelete: false },
  { x: 1, y: 30, type: "house", toDelete: false },
  { x: 22, y: 10, type: "tree", toDelete: true },
  { x: 5, y: 34, type: "rock", toDelete: true },
  null,
  { x: 19, y: 40, type: "tree", toDelete: false },
  { x: 35, y: 35, type: "house", toDelete: false },
  { x: 19, y: 40, type: "tree", toDelete: true },
  { x: 24, y: 31, type: "rock", toDelete: false },
];

objectArray.forEach((item, index, array) => {
  if (item && item.toDelete === true) {
    array[index] = null;
  }
});

console.log(objectArray);
// Erase the entries by generating a new array with Array.map where the objects to be deleted have been replaced with null and the rest stay as-is. i do not understand the point of using map and filter together, but i did it as the task asked to use map().
const cleanedArray = objectArray
  .map((item) => {
    if (item === null) {
      return null;
    }

    return item;
  })
  .filter((item) => item !== null);
console.log(cleanedArray);

// c) performance wise A would be better, as it is not generating a new array, but modifying the existing one. not to mention 2nd makes 2 new arrays, one with map and one with filter.
