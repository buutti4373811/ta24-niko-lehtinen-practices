const arr = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];
//a
for (const animal of arr) {
  if (animal.includes("e")) {
    console.log(animal);
  }
}

arr.forEach((animal) => {
  if (animal.includes("e")) {
    console.log(animal);
  }
});

