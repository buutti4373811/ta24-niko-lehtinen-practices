const str =
  "According to all known laws of aviation, there is no way a bee should be able to fly.";

const words = str.split(" ");

for (let word of words) {
  // exercise doesnt specify if the word should be case sensitive but here is a solution for not case sensitive
  if (word.charAt(0).toUpperCase() === "A") {
    console.log(word);
  }
}
