const numbers = [4, 7, 1, 8, 5];

///a)
const incrementAll = (arr: number[]): number[] => {
  const incrementAllArray = arr.map((value) => value + 1);

  console.log("A)", incrementAllArray);
  return incrementAllArray;
};
///b)
const decrementAll = (arr: number[]): number[] => {
  const decrementAllArray = arr.map((value) => value - 1);

  console.log("B)", decrementAllArray);
  return decrementAllArray;
};

incrementAll(numbers);
decrementAll(numbers);
