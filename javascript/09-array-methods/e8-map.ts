{
  const arr = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

  const mappedArray = arr.map((animal) => {
    return animal.length;
  });
  console.log("exercise 8a below:");

  console.log(mappedArray);
  const hasOSecondLetterArray = arr
    .map((animal) => {
      if (animal[1] === "o") {
        return animal;
      }
    })
    .filter(Boolean);
  console.log("exercise 8b below:");
  console.log(hasOSecondLetterArray);

  console.log("original array below unchanged:");
  console.log(arr);
}
