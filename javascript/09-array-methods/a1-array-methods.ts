const numbers = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50];
//a)
for (let i = 0; i < numbers.length; i++) {
  if (numbers[i] > 20) {
    console.log("A)", numbers[i]);
    break;
  }
}

//b)
numbers.find((number) => {
  if (number > 20) {
    console.log("B)", number);
    return true;
  }
});

//c)
const index = numbers.findIndex((number) => {
  if (number > 20) {
    return true;
  }
});
console.log("C)", index);

//d)
const splicedArray = numbers.splice(0, index);
console.log("D)", splicedArray);
