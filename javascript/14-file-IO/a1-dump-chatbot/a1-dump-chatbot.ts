import readline from "readline-sync";

console.log(
  "Hi! I am a dumb chat bot. You can check all the things I can do by typing 'help'."
);

const commands = {
  help: "Here's a list of commands that I can execute!\n\nhelp: Opens this dialog.\nhello: I will say hello to you\nbotInfo: I will introduce myself\nbotName: I will tell my name\nbotRename: You can rename me\nforecast: I will forecast tomorrow's weather 100% accurately\nquit: Quits the program.",
  hello: "Hello!",
  botInfo: "I am a dumb chat bot created by you.",
  botName: "My name is ChatBot.",
  botRename: () => {
    const newName = readline.question("Enter a new name for me: ");
    console.log(`Thanks! My new name is ${newName}.`);
  },
  forecast: "Tomorrow's weather: Sunny with a chance of rainbows.",
  quit: "Goodbye! See you later.",
};

while (true) {
  const userInput = readline.question("\nEnter a command: ");
  if (userInput.toLowerCase() === "quit") {
    console.log(commands.quit);
    break;
  } else if (commands[userInput.toLowerCase() as keyof typeof commands]) {
    const command = commands[userInput.toLowerCase() as keyof typeof commands];
    if (typeof command === "function") {
      command();
    } else {
      console.log(command);
    }
  } else {
    console.log("Invalid command. Type 'help' to see available commands.");
  }
}
