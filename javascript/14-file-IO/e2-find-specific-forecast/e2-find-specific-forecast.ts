import fs from "fs";

const forecast = {
  day: "monday",
  temperature: 20,
  cloudy: true,
  sunny: false,
  windy: false,
};
const data = JSON.stringify(forecast);
fs.writeFileSync("forecast_data.json", data, "utf8");

const data2 = fs.readFileSync("forecast_data.json", "utf8");
const allForecasts = JSON.parse(data2);

allForecasts.temperature = 25;
console.log(allForecasts);

const newData = JSON.stringify(allForecasts);
fs.writeFileSync("forecast_data.json", newData, "utf8");
