import fs from "fs";

const readStream = fs.createReadStream("./textFile.txt", "utf-8");
readStream.on("data", (txt) => {
  const words = txt.toString().toLowerCase().split(" ");

  const newWords = words.map((word) => {
    if (word === "joulu") {
      word = "kinkku";
      return word;
    } else if (word === "lapsilla") {
      word = "poroilla";
      return word;
    }
    return word;
  });
  const newString = newWords.join(" ");
  console.log(newWords);

  const stream = fs.createWriteStream("./writeStream.txt");
  stream.write(newString, (err) => {
    if (err) console.log(err);
    else console.log("success");
  });
});
