const fruit1 = {
  name: "pearl",
  weight: 178,
};
const fruit2 = {
  name: "lemon",
  weight: 120,
};
const fruit3 = {
  name: "apple",
  weight: 90,
};
const fruit4 = {
  name: "mango",
  weight: 134,
};

const avgWeight =
  (fruit1.weight + fruit2.weight + fruit3.weight + fruit4.weight) / 4;

console.log(avgWeight);

// pienin erotus keskiarvosta
const fruit1Diff = avgWeight - fruit1.weight;
const fruit2Diff = avgWeight - fruit2.weight;
const fruit3Diff = avgWeight - fruit3.weight;
const fruit4Diff = avgWeight - fruit4.weight;

console.log(fruit1Diff, fruit2Diff, fruit3Diff, fruit4Diff);

let closestDiff = fruit1Diff;
let closestFruit = fruit1.name;

if (Math.abs(fruit2Diff) < Math.abs(closestDiff)) {
  closestDiff = fruit2Diff;
  closestFruit = fruit2.name;
}

if (Math.abs(fruit3Diff) < Math.abs(closestDiff)) {
  closestDiff = fruit3Diff;
  closestFruit = fruit3.name;
}

if (Math.abs(fruit4Diff) < Math.abs(closestDiff)) {
  closestDiff = fruit4Diff;
  closestFruit = fruit4.name;
}

console.log(`The ${closestFruit} has the weight closest to the average.`);
