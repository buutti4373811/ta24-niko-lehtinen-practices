let balance = 1000;
const checkBalance = true;
const isActive = false;

if (checkBalance) {
  if (isActive && balance > 0) {
    console.log("your balance is " + balance);
  } else if (!isActive) {
    console.log("Your account is not active");
  } else if (balance === 0) {
    console.log("Your account is empty");
  } else {
    console.log("Your balance is negative. ");
  }
} else {
  console.log("Thank you. Have a nice day!");
}
