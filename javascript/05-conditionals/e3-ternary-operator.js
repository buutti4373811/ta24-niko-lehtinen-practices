const age = 19;
const ageLimit = 18;

const shallPass = age >= ageLimit ? "You shall pass" : "You shall not pass";
console.log(shallPass);
