const name1 = "David";
const name2 = "John";
const name3 = "Jennifer";

if (name1.length > name2.length && name1.length > name3.length) {
  if (name2.length > name3.length) {
    console.log(name1, name2, name3);
  } else {
    console.log(name1, name3, name2);
  }
} else if (name2.length > name1.length && name2.length > name3.length) {
  if (name1.length > name3.length) {
    console.log(name2, name1, name3);
  } else {
    console.log(name2, name3, name1);
  }
} else if (name3.length > name1.length && name3.length > name2.length) {
  if (name1.length > name2.length) {
    console.log(name3, name1, name2);
  } else {
    console.log(name3, name2, name1);
  }
}
