// i did not solve the math formula to calculate the power usage correctly because 0.42 and 0.57 are not the correct values for the time / power usage of the computers. I will use the given values to solve the problem.
const pc1PowerUsage = 600 * 0.42;
const pc2PowerUsage = 480 * 0.57;

const lessPowerUsed =
  pc1PowerUsage > pc2PowerUsage
    ? "computer 1 used less power"
    : "computer 2 used less power";

console.log(lessPowerUsed);
