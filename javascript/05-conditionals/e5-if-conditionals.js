const playerCount = 4;

if (playerCount === 4) {
  console.log("he game of Hearts can only be played with 4 people");
}

const isStressed = true;
const hasIceCream = true;

if (!isStressed && hasIceCream) {
  console.log("Mark is happy");
}

const sunny = true;
const raining = false;
const temp = 25;

if (sunny && !raining && temp >= 20) {
  console.log("It's a beach day!");
}

const suzyOnTuesday = false;
const danOnTuesday = false;

if ((suzyOnTuesday || danOnTuesday) && !(danOnTuesday && suzyOnTuesday)) {
  console.log("arin is happy");
} else {
  console.log("arin is sad");
}
