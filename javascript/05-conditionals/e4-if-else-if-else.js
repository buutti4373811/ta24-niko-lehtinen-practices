const booly1 = true;
const booly2 = false;

if (booly1 && booly2) {
  console.log("both are true");
}
if (booly1 && !booly2) {
  console.log("first is true and second is false");
}
if (!booly1 && booly2) {
  console.log("first is false and second is true");
}
if (!booly1 && !booly2) {
  console.log("both are false");
}
