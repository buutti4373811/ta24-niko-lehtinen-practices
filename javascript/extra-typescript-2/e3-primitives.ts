const string = (str: string): { length: number; words: number } => {
  return {
    length: str.length,
    words: str.split(" ").length,
  };
};

console.log(string("Hello World"));
