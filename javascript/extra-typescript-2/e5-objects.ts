interface Boat {
  hullBreached: boolean;
  fillLevel: number;
  isItSinking: () => void;
  sunk?: boolean;
}

const boat: Boat = {
  hullBreached: true,
  fillLevel: 0,
  isItSinking: function () {
    if (boat.hullBreached) {
      const intervalId = setInterval(() => {
        if (boat.fillLevel < 100) {
          boat.fillLevel += 20;
          console.log(`Fill level: ${boat.fillLevel}%`);
        } else {
          boat.sunk = true;
          console.log("The boat has sunk!", boat);
          clearInterval(intervalId);
        }
      }, 1000);
    } else {
      console.log("The boat is not sinking!", boat);
    }
  },
};
boat.hullBreached = false;
boat.isItSinking();

boat.hullBreached = true;
boat.isItSinking();
