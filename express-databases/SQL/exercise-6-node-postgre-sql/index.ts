import express from "express";
import { createProductsTable } from "./db";
import {
  deleteProduct,
  getAllProducts,
  getProductById,
  insertProduct,
  updateProduct,
} from "./dao";
import bodyParser from "body-parser";

const server = express();

createProductsTable();

const { PORT } = process.env;
server.use(bodyParser.json());

server.post("/product", async (req: express.Request, res: express.Response) => {
  const product = await req.body;

  const results = await insertProduct(product);
  const storedProduct = { id: results.rows[0], ...product };

  res.send(storedProduct);
});

server.get(
  "/product/:id",
  async (req: express.Request, res: express.Response) => {
    const id = parseInt(req.params.id);
    const product = await getProductById(id);

    if (!product) {
      res.status(404).send("Product not found");
    } else {
      res.send(product);
    }
  }
);

server.put(
  "/product/:id",
  async (req: express.Request, res: express.Response) => {
    const id = parseInt(req.params.id);

    const { name, price } = req.body;
    if (!name || !price) {
      return res.status(400).send({ error: "Name or price is required" });
    }
    const updatedProduct = await updateProduct(id, name, price);
    res.send(updatedProduct);
  }
);

server.delete(
  "/product/:id",
  async (req: express.Request, res: express.Response) => {
    const id = parseInt(req.params.id);

    await deleteProduct(id);

    res.sendStatus(204);
  }
);

server.get("/products", async (req: express.Request, res: express.Response) => {
  const products = await getAllProducts();
  console.log(products);
  res.send(products);
});

server.listen(PORT, () => {
  console.log("Products API listening to port", PORT);
});
