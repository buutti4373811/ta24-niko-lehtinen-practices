import { executeQuery } from "./db";

export const insertProduct = async (product: {
  name: string;
  price: number;
}) => {
  const id = Math.floor(Math.random() * 100000) + 1;

  const params = [id, ...Object.values(product)];
  const query = `
    INSERT INTO products (id, name, price)
    VALUES ($1, $2, $3)
  
  `;

  console.log(`insertProduct: ${params[0]}`);

  const results = await executeQuery(query, params);
  console.log(`new product ${id} inserted`);
  return results;
};

export const getProductById = async (id: number) => {
  const query = `
    SELECT * FROM products WHERE id = $1
  `;

  const results = await executeQuery(query, [id]);
  console.log(results.rows[0]);
  return results.rows[0] || null;
};

export const updateProduct = async (
  id: number,
  name: string,
  price: number
) => {
  const query = `
    UPDATE products
    SET name = $2, price = $3
    WHERE id = $1
    RETURNING *
  `;

  const results = await executeQuery(query, [id, name, price]);
  return results.rows[0];
};

export const deleteProduct = async (id: number) => {
  const query = `
    DELETE FROM products WHERE id = $1
  `;

  const results = await executeQuery(query, [id]);
  return results.rowCount;
};

export const getAllProducts = async () => {
  const query = `
    SELECT * FROM products
  `;

  const results = await executeQuery(query);
  return results.rows;
};
