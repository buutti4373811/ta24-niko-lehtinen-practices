import express from "express";

const server = express();

server.get("/", (request, response) => {
  response.send("Just saying hello!");
});
server.get("/endpoint2", (req, res) => {
  res.send("This is the second endpoint! exercise 2");
});
server.get("/:name/:surname", (request, response) => {
  console.log("Params:");
  console.log(request.params);
  console.log("Query:");
  console.log(request.query);

  response.send("Hello " + request.params.name);
});

server.listen(5000, () => {
  console.log("Listening to port 5000");
});
