import express from "express";
import { Response } from "express";

interface Counters {
  [name: string]: number;
}

const server = express();
let counter = 0;

server.get("/counter", (req: express.Request, res: Response) => {
  counter++;
  const { number } = req.query;
  if (number) {
    counter = parseInt(number as string);
  }
  const responseObj = {
    counter: counter,
  };

  res.json(responseObj);
});

const counters: Counters = {};
server.get("/counter/:name", (req: express.Request, res: Response) => {
  const name = req.params.name;
  if (counters[name] === undefined || counters[name] === null) {
    counters[name] = 0;
  }

  counters[name]++;

  const responseMessage = `${name} was here ${counters[name]} times`;

  res.send(responseMessage);
});

server.listen(5000, () => {
  console.log("Listening to port 5000");
});
