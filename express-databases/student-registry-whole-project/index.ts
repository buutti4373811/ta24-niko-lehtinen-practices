import express from "express";
import { logger, unknownEndpoint } from "./middleware";
import studentRouter from "./studentRouter";
import userRouter from "./userRouter";

const app = express();
// Exercice 7 below 3 lines
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger);
app.use(express.static("public"));
app.use(studentRouter);
app.use(userRouter);

app.use(unknownEndpoint);
app.listen(5000, () => {
  console.log("Listening to port 5000");
});
export default app;
