import "dotenv/config";
import jwt from "jsonwebtoken";

const payload = { username: "sugarplumfairy" };
const secret = process.env.SECRET;
const options = { expiresIn: "15min" };

if (!secret) {
  throw new Error("No secret provided");
}
const token = jwt.sign(payload, secret, options);

jwt.verify(token, secret, (err, decoded) => {
  if (err) {
    console.error("Invalid token:", err.message);
  } else {
    console.log("Decoded token:", decoded);
  }
});
