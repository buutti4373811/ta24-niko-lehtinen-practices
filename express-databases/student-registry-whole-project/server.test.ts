import request from "supertest";
import jwt from "jsonwebtoken";

import { describe, it, expect } from "@jest/globals";
import app from ".";

describe("student router", () => {
  it("Returns 404 on invalid address", async () => {
    const response = await request(app).get("/invalidaddress");
    expect(response.statusCode).toBe(404);
  });
  it("Returns 404 on valid address but not authenticated", async () => {
    const response = await request(app).get("/students");
    expect(response.statusCode).toBe(401);
  });

  it("should return 200 when authenticated with valid token", async () => {
    const secret = process.env.SECRET;
    const tokenPayload = { username: "testUser" };
    const validToken = jwt.sign(tokenPayload, secret as string);

    const response = await request(app)
      .get("/students")
      .set("Authorization", `Bearer ${validToken}`);

    expect(response.status).toBe(200);
  });

  it("should return 201 if authenticated admin user", async () => {
    // Generate a valid JWT token for an admin user
    const secret = process.env.SECRET;
    const tokenPayload = { username: "admin" };
    const validToken = jwt.sign(tokenPayload, secret as string);

    // Make a POST request to the /student endpoint with the valid token
    const response = await request(app)
      .post("/student")
      .set("Authorization", `Bearer ${validToken}`)
      .send({ id: "some-id", name: "John Doe", email: "john@example.com" });

    // Expect a status code of 201 indicating successful creation
    expect(response.status).toBe(201);
  });

  it("should return 403 if authenticated non-admin user", async () => {
    // Generate a valid JWT token for a non-admin user
    const secret = process.env.SECRET;
    const tokenPayload = { username: "regular_user" };
    const validToken = jwt.sign(tokenPayload, secret as string);

    // Make a POST request to the /student endpoint with the valid token
    const response = await request(app)
      .post("/student")
      .set("Authorization", `Bearer ${validToken}`)
      .send({ id: "some-id", name: "John Doe", email: "john@example.com" });

    // Expect a status code of 403 indicating unauthorized access
    expect(response.status).toBe(403);
  });

  it("should return 401 if not authenticated", async () => {
    // Make a POST request to the /student endpoint without authentication
    const response = await request(app)
      .post("/student")
      .send({ id: "some-id", name: "John Doe", email: "john@example.com" });

    // Expect a status code of 401 indicating unauthenticated access
    expect(response.status).toBe(401);
  });

  it("should return 404 if student ID not found but authenticated", async () => {
    const secret = process.env.SECRET;
    const tokenPayload = { username: "testUser" };
    const validToken = jwt.sign(tokenPayload, secret as string);

    const response = await request(app)
      .get("/student/:id")
      .set("Authorization", `Bearer ${validToken}`);

    expect(response.status).toBe(404);
    expect(response.body).toEqual({ error: "Student not found" });
  });
});
