import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

interface AuthenticatedRequest extends express.Request {
  user?: any; // Change 'any' to the type of your user object if available
}

export const logger = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  console.log("Logging request");
  console.log("Date:", new Date().toISOString());
  console.log("Method:", req.method);
  console.log(req.url);

  if (req.body && Object.keys(req.body).length > 0)
    console.log("Body:", req.body);

  next();
};

export const unknownEndpoint = (
  _req: express.Request,
  res: express.Response
) => {
  res.status(404).send({ error: "No one here" });
};
export const authenticate = (
  req: AuthenticatedRequest,
  res: express.Response,
  next: express.NextFunction
) => {
  const auth = req.get("Authorization");
  if (!auth?.startsWith("Bearer ")) {
    return res.status(401).send("Invalid token");
  }
  const token = auth.substring(7);
  const secret = process.env.SECRET;

  if (!secret) {
    throw new Error("No secret provided");
  }

  try {
    const decodedToken = jwt.verify(token, secret);
    req.user = decodedToken;
    next();
  } catch (error) {
    return res.status(401).send("Invalid token");
  }
};

export const isAdmin = (
  req: AuthenticatedRequest,
  res: express.Response,
  next: express.NextFunction
) => {
  const auth = req.get("Authorization");
  if (!auth?.startsWith("Bearer ")) {
    return res.status(401).send("Invalid token");
  }

  const token = auth.substring(7);
  const secret = process.env.SECRET;

  if (!secret) {
    throw new Error("No secret provided");
  }

  try {
    const decodedToken: jwt.JwtPayload = jwt.verify(
      token,
      secret
    ) as jwt.JwtPayload;
    if (decodedToken.username !== "admin") {
      return res.status(403).send("Unauthorized");
    }
    req.user = decodedToken;
    next();
  } catch (error) {
    return res.status(401).send("Invalid token");
  }
};
