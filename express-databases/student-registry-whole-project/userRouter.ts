import express from "express";
import argon2 from "argon2";
import "dotenv/config";
import jwt from "jsonwebtoken";

interface User {
  username: string;
  password: string;
}

const userRouter = express.Router();

interface User {
  username: string;
  password: string;
}

const users: User[] = [];

userRouter.post(
  "/register",
  async (req: express.Request, res: express.Response) => {
    const { username, password } = req.body;

    if (!username || !password) {
      return res.status(400).send({ error: "Missing parameters" });
    }
    const hashedPassword = await argon2.hash(password);
    users.push({ username, password: hashedPassword });

    const payload = { username };
    const secret = process.env.SECRET;
    const options = { expiresIn: "15min" };

    if (!secret) {
      throw new Error("No secret provided");
    }
    const token = jwt.sign(payload, secret, options);

    res.status(200).send({ token });
  }
);

userRouter.post(
  "/login",
  async (req: express.Request, res: express.Response) => {
    const { username, password } = req.body;

    if (!username || !password) {
      return res.status(400).send({ error: "Missing parameters" });
    }

    const user = users.find((user) => user.username === username);

    if (!user) {
      return res.status(401).send({ error: "Invalid username" });
    }

    const validPassword = await argon2.verify(user.password, password);

    if (!validPassword) {
      return res.status(401).send({ error: "unauthorizes" });
    }
    const payload = { username };
    const secret = process.env.SECRET;
    const options = { expiresIn: "15min" };

    if (!secret) {
      throw new Error("No secret provided");
    }
    const token = jwt.sign(payload, secret, options);

    res.status(200).send({ token });
  }
);

userRouter.post(
  "/admin",
  async (req: express.Request, res: express.Response) => {
    const { username, password } = req.body;

    if (username !== process.env.ADMIN_USERNAME) {
      return res.status(401).send({ error: "Unauthorized" });
    }

    const passwordMatchesHash = await argon2.verify(
      process.env.ADMIN_PASSWORD_HASH as string,
      password
    );

    if (!passwordMatchesHash) {
      return res.status(401).send({ error: "Unauthorized" });
    }

    const payload = { username };
    const secret = process.env.ADMIN_USERNAME;
    const options = { expiresIn: "15min" };

    if (!secret) {
      throw new Error("No secret provided");
    }
    const token = jwt.sign(payload, secret, options);
    res.status(200).send({ token });
  }
);

export default userRouter;
