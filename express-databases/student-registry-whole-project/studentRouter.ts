import express from "express";
import { authenticate, isAdmin } from "./middleware";
const studentRouter = express.Router();

// This is the student array that is used on all requests, requests modify this array to add, update or delete students they do not create new arrays
let students: { id: string; name: string; email: string }[] = [];

// Request to get all students id:s
studentRouter.get(
  "/students",
  authenticate,
  (req: express.Request, res: express.Response) => {
    const studentIds = students.map((student) => student.id);
    res.send(studentIds);
  }
);

// Exercice 8 below

// Add student to the students array with a POST request
studentRouter.post(
  "/student",
  authenticate,
  isAdmin,

  (req: express.Request, res: express.Response) => {
    // Extract student information from request body
    const { id, name, email } = req.body;

    // Check if any parameter is missing
    if (!id || !name || !email) {
      return res.status(400).send({ error: "Missing parameters" });
    }
    students.push({ id, name, email });
    console.log(students);
    res.status(201).send();
  }
);
// Get single student by id Request

studentRouter.get(
  "/student/:id",
  authenticate,
  (req: express.Request, res: express.Response) => {
    const student = students.find(
      (student) => student.id.toString() === req.params.id
    );
    if (!student) {
      return res.status(404).send({ error: "Student not found" });
    }

    res.send(student);
  }
);

// Exercice 9 below
// Update student information with a PUT request
studentRouter.put(
  "/student/:id",
  authenticate,
  isAdmin,
  (req: express.Request, res: express.Response) => {
    const { name, email } = req.body;

    // Check if any parameter is missing
    if (!name && !email) {
      return res.status(400).send({ error: "Name or email is required" });
    }

    const student = students.find(
      (student) => student.id.toString() === req.params.id
    );
    // Check if student exists
    if (!student) {
      return res.status(404).send({ error: "Student not found" });
    }
    // Update student information
    if (name) student.name = name;
    if (email) student.email = email;

    res.sendStatus(204);
  }
);

// Delete student with a DELETE request
studentRouter.delete(
  "/student/:id",
  authenticate,
  isAdmin,
  (req: express.Request, res: express.Response) => {
    const studentId = req.params.id;

    // Find the index of the student in the students array
    const index = students.findIndex(
      (student) => student.id.toString() === studentId
    );
    console.log(index, students);
    // If student not found, return 404 error
    if (index === -1) {
      return res.status(404).send({ error: "Student not found" });
    }

    // Remove the student from the students array
    students.splice(index, 1);

    res.sendStatus(204); // Return empty response with status code 204
  }
);

export default studentRouter;
